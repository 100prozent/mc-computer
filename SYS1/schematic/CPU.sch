EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "RDK SYS1 Z80 Board"
Date "2020-10-14"
Rev "v0.2"
Comp ""
Comment1 "v0.2: ch. Resistor Numbering"
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L CPU:Z80CPU U1
U 1 1 5EF10593
P 9550 4325
AR Path="/5EF1043C/5EF10593" Ref="U1"  Part="1" 
AR Path="/5EFDB5C5/5EF10593" Ref="U?"  Part="1" 
AR Path="/5EFDD352/5EF10593" Ref="U?"  Part="1" 
F 0 "U1" H 9050 5800 50  0000 C CNN
F 1 "Z80CPU" H 9150 5725 50  0000 C CNN
F 2 "" H 9550 4725 50  0001 C CNN
F 3 "www.zilog.com/manage_directlink.php?filepath=docs/z80/um0080" H 9550 4725 50  0001 C CNN
	1    9550 4325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EFDE4A1
P 9550 2825
AR Path="/5EF1043C/5EFDE4A1" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5EFDE4A1" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5EFDE4A1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9550 2675 50  0001 C CNN
F 1 "+5V" H 9565 2998 50  0000 C CNN
F 2 "" H 9550 2825 50  0001 C CNN
F 3 "" H 9550 2825 50  0001 C CNN
	1    9550 2825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF15018
P 9550 5825
AR Path="/5EF1043C/5EF15018" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5EF15018" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5EF15018" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9550 5575 50  0001 C CNN
F 1 "GND" H 9555 5652 50  0000 C CNN
F 2 "" H 9550 5825 50  0001 C CNN
F 3 "" H 9550 5825 50  0001 C CNN
	1    9550 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 3125 10525 3125
Wire Wire Line
	10250 3225 10525 3225
Wire Wire Line
	10250 3325 10525 3325
Wire Wire Line
	10250 3425 10525 3425
Wire Wire Line
	10250 3525 10525 3525
Wire Wire Line
	10250 3625 10525 3625
Wire Wire Line
	10250 3725 10525 3725
Wire Wire Line
	10250 3825 10525 3825
Wire Wire Line
	10250 3925 10525 3925
Wire Wire Line
	10250 4025 10525 4025
Wire Wire Line
	10250 4125 10525 4125
Wire Wire Line
	10250 4225 10525 4225
Wire Wire Line
	10250 4325 10525 4325
Wire Wire Line
	10250 4425 10525 4425
Wire Wire Line
	10250 4525 10525 4525
Wire Wire Line
	10250 4625 10525 4625
Wire Wire Line
	10250 4825 10525 4825
Wire Wire Line
	10250 4925 10525 4925
Wire Wire Line
	10250 5025 10525 5025
Wire Wire Line
	10250 5125 10525 5125
Wire Wire Line
	10250 5225 10525 5225
Wire Wire Line
	10250 5325 10525 5325
Wire Wire Line
	10250 5425 10525 5425
Wire Wire Line
	10250 5525 10525 5525
Text Label 10525 3125 2    50   ~ 0
A0
Text Label 10525 3225 2    50   ~ 0
A1
Text Label 10525 3325 2    50   ~ 0
A2
Text Label 10525 3425 2    50   ~ 0
A3
Text Label 10525 3525 2    50   ~ 0
A4
Text Label 10525 3625 2    50   ~ 0
A5
Text Label 10525 3725 2    50   ~ 0
A6
Text Label 10525 3825 2    50   ~ 0
A7
Text Label 10525 3925 2    50   ~ 0
A8
Text Label 10525 4025 2    50   ~ 0
A9
Text Label 10525 4125 2    50   ~ 0
A10
Text Label 10525 4225 2    50   ~ 0
A11
Text Label 10525 4325 2    50   ~ 0
A12
Text Label 10525 4425 2    50   ~ 0
A13
Text Label 10525 4525 2    50   ~ 0
A14
Text Label 10525 4625 2    50   ~ 0
A15
Text Label 10525 4825 2    50   ~ 0
D0
Text Label 10525 4925 2    50   ~ 0
D1
Text Label 10525 5025 2    50   ~ 0
D2
Text Label 10525 5125 2    50   ~ 0
D3
Text Label 10525 5225 2    50   ~ 0
D4
Text Label 10525 5325 2    50   ~ 0
D5
Text Label 10525 5425 2    50   ~ 0
D6
Text Label 10525 5525 2    50   ~ 0
D7
$Comp
L power:+5V #PWR?
U 1 1 5EFDE4A7
P 8275 3950
AR Path="/5EF1043C/5EFDE4A7" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5EFDE4A7" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5EFDE4A7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8275 3800 50  0001 C CNN
F 1 "+5V" H 8290 4123 50  0000 C CNN
F 2 "" H 8275 3950 50  0001 C CNN
F 3 "" H 8275 3950 50  0001 C CNN
	1    8275 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5EFDE4A8
P 8275 4050
AR Path="/5EF1043C/5EFDE4A8" Ref="R8"  Part="1" 
AR Path="/5EFDB5C5/5EFDE4A8" Ref="R?"  Part="1" 
AR Path="/5EFDD352/5EFDE4A8" Ref="R?"  Part="1" 
F 0 "R8" H 8334 4096 50  0000 L CNN
F 1 "1k" H 8334 4005 50  0000 L CNN
F 2 "" H 8275 4050 50  0001 C CNN
F 3 "~" H 8275 4050 50  0001 C CNN
	1    8275 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8275 4150 8275 4325
Wire Wire Line
	8275 4325 8850 4325
$Comp
L Device:R_Small R6
U 1 1 5EF197D5
P 8275 5075
AR Path="/5EF1043C/5EF197D5" Ref="R6"  Part="1" 
AR Path="/5EFDB5C5/5EF197D5" Ref="R?"  Part="1" 
AR Path="/5EFDD352/5EF197D5" Ref="R?"  Part="1" 
F 0 "R6" H 8334 5121 50  0000 L CNN
F 1 "1k" H 8334 5030 50  0000 L CNN
F 2 "" H 8275 5075 50  0001 C CNN
F 3 "~" H 8275 5075 50  0001 C CNN
	1    8275 5075
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF197E2
P 8275 4975
AR Path="/5EF1043C/5EF197E2" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5EF197E2" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5EF197E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8275 4825 50  0001 C CNN
F 1 "+5V" H 8290 5148 50  0000 C CNN
F 2 "" H 8275 4975 50  0001 C CNN
F 3 "" H 8275 4975 50  0001 C CNN
	1    8275 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	8275 5425 8275 5175
Wire Wire Line
	8275 5425 8850 5425
Wire Wire Line
	8575 5125 8850 5125
Wire Wire Line
	8575 5025 8850 5025
Wire Wire Line
	8575 4425 8850 4425
Wire Wire Line
	8575 4225 8850 4225
Wire Wire Line
	8575 4125 8850 4125
Wire Wire Line
	8575 4925 8850 4925
Wire Wire Line
	8575 4825 8850 4825
Wire Wire Line
	8575 5525 8850 5525
Text Label 8575 3725 0    50   ~ 0
~NMI
Text Label 8575 3825 0    50   ~ 0
~INT
Text Label 8575 4425 0    50   ~ 0
~HALT
Text Label 8575 5025 0    50   ~ 0
~MREQ
Text Label 8575 5125 0    50   ~ 0
~IORQ
Text Label 8575 4225 0    50   ~ 0
~RFSH
Text Label 8575 4125 0    50   ~ 0
~M1
Text Label 8575 4825 0    50   ~ 0
~RD
Text Label 8575 4925 0    50   ~ 0
~WR
Text Label 8575 5425 0    50   ~ 0
~BUSRQ
Text Label 8575 5525 0    50   ~ 0
~BUSACK
Text Label 8575 4325 0    50   ~ 0
~WAIT
Text Label 8575 3125 0    50   ~ 0
~RESET
Wire Wire Line
	1600 1275 1275 1275
Wire Wire Line
	1600 2075 1275 2075
Wire Wire Line
	1600 1775 1275 1775
Wire Wire Line
	1600 1475 1275 1475
Wire Wire Line
	1600 1675 1275 1675
Wire Wire Line
	1600 1975 1275 1975
Wire Wire Line
	1600 2275 1275 2275
Wire Wire Line
	1600 1175 1275 1175
Wire Wire Line
	1600 1375 1275 1375
Text Label 1600 1775 2    50   ~ 0
~RD
Text Label 1600 1675 2    50   ~ 0
~HALT
Text Label 1600 1975 2    50   ~ 0
~MREQ
Text Label 1600 2075 2    50   ~ 0
~IORQ
Text Label 1600 1375 2    50   ~ 0
~M1
Text Label 1600 2175 2    50   ~ 0
~BUSRQ
Text Label 1600 1175 2    50   ~ 0
~NMI
Text Label 1600 1575 2    50   ~ 0
~WAIT
Text Label 1600 2275 2    50   ~ 0
~BUSACK
Text Label 1600 1475 2    50   ~ 0
~RFSH
Text Label 1600 1275 2    50   ~ 0
~INT
Wire Wire Line
	1600 975  1275 975 
Wire Wire Line
	1600 1875 1275 1875
Text Label 1600 1875 2    50   ~ 0
~WR
Text Label 1600 975  2    50   ~ 0
~RESET
Wire Wire Line
	1600 1575 1275 1575
Wire Wire Line
	1600 2175 1275 2175
Wire Wire Line
	2025 2200 2300 2200
Wire Wire Line
	2025 1100 2300 1100
Wire Wire Line
	2600 1700 2875 1700
Wire Wire Line
	2600 2000 2875 2000
Wire Wire Line
	2025 2100 2300 2100
Wire Wire Line
	2025 800  2300 800 
Wire Wire Line
	2025 1500 2300 1500
Wire Wire Line
	2025 1600 2300 1600
Wire Wire Line
	2025 900  2300 900 
Wire Wire Line
	2025 1200 2300 1200
Wire Wire Line
	2025 1900 2300 1900
Wire Wire Line
	2025 1400 2300 1400
Wire Wire Line
	2600 1800 2875 1800
Wire Wire Line
	2025 1700 2300 1700
Text Label 2300 1200 2    50   ~ 0
A4
Wire Wire Line
	2600 2200 2875 2200
Text Label 2300 1300 2    50   ~ 0
A5
Wire Wire Line
	2025 2000 2300 2000
Wire Wire Line
	2600 1600 2875 1600
Wire Wire Line
	2600 1900 2875 1900
Wire Wire Line
	2025 1000 2300 1000
Wire Wire Line
	2025 1800 2300 1800
Text Label 2875 1800 2    50   ~ 0
D2
Wire Wire Line
	2600 2300 2875 2300
Text Label 2300 1900 2    50   ~ 0
A11
Text Label 2875 1700 2    50   ~ 0
D1
Text Label 2875 1600 2    50   ~ 0
D0
Text Label 2300 1600 2    50   ~ 0
A8
Text Label 2300 1400 2    50   ~ 0
A6
Text Label 2300 1500 2    50   ~ 0
A7
Wire Wire Line
	2025 2300 2300 2300
Text Label 2875 1900 2    50   ~ 0
D3
Text Label 2300 1800 2    50   ~ 0
A10
Text Label 2300 2300 2    50   ~ 0
A15
Text Label 2875 2000 2    50   ~ 0
D4
Text Label 2875 2300 2    50   ~ 0
D7
Text Label 2875 2100 2    50   ~ 0
D5
Text Label 2875 2200 2    50   ~ 0
D6
Text Label 2300 2100 2    50   ~ 0
A13
Text Label 2300 2200 2    50   ~ 0
A14
Text Label 2300 1000 2    50   ~ 0
A2
Text Label 2300 800  2    50   ~ 0
A0
Text Label 2300 1100 2    50   ~ 0
A3
Text Label 2300 2000 2    50   ~ 0
A12
Text Label 2300 900  2    50   ~ 0
A1
Wire Wire Line
	2025 1300 2300 1300
Text Label 2300 1700 2    50   ~ 0
A9
Wire Wire Line
	2600 2100 2875 2100
Text HLabel 2600 1600 0    50   Input ~ 0
D0
Text HLabel 2600 1700 0    50   Input ~ 0
D1
Text HLabel 2600 1800 0    50   Input ~ 0
D2
Text HLabel 2600 1900 0    50   Input ~ 0
D3
Text HLabel 2600 2000 0    50   Input ~ 0
D4
Text HLabel 2600 2100 0    50   Input ~ 0
D5
Text HLabel 2600 2200 0    50   Input ~ 0
D6
Text HLabel 2600 2300 0    50   Input ~ 0
D7
Text HLabel 2025 800  0    50   Input ~ 0
A0
Text HLabel 2025 900  0    50   Input ~ 0
A1
Text HLabel 2025 1000 0    50   Input ~ 0
A2
Text HLabel 2025 1100 0    50   Input ~ 0
A3
Text HLabel 2025 1200 0    50   Input ~ 0
A4
Text HLabel 2025 1300 0    50   Input ~ 0
A5
Text HLabel 2025 1400 0    50   Input ~ 0
A6
Text HLabel 2025 1500 0    50   Input ~ 0
A7
Text HLabel 2025 1600 0    50   Input ~ 0
A8
Text HLabel 2025 1700 0    50   Input ~ 0
A9
Text HLabel 2025 1800 0    50   Input ~ 0
A10
Text HLabel 2025 1900 0    50   Input ~ 0
A11
Text HLabel 2025 2000 0    50   Input ~ 0
A12
Text HLabel 2025 2100 0    50   Input ~ 0
A13
Text HLabel 2025 2200 0    50   Input ~ 0
A14
Text HLabel 2025 2300 0    50   Input ~ 0
A15
Text HLabel 1275 975  0    50   Input ~ 0
~RESET
Text HLabel 1275 1175 0    50   Input ~ 0
~NMI
Text HLabel 1275 1275 0    50   Input ~ 0
~INT
Text HLabel 1275 1375 0    50   Input ~ 0
~M1
Text HLabel 1275 1475 0    50   Input ~ 0
~RFSH
Text HLabel 1275 1575 0    50   Input ~ 0
~WAIT
Text HLabel 1275 1675 0    50   Input ~ 0
~HALT
Text HLabel 1275 1775 0    50   Input ~ 0
~RD
Text HLabel 1275 1875 0    50   Input ~ 0
~WR
Text HLabel 1275 1975 0    50   Input ~ 0
~MREQ
Text HLabel 1275 2075 0    50   Input ~ 0
~IORQ
Text HLabel 1275 2175 0    50   Input ~ 0
~BUSRQ
Text HLabel 1275 2275 0    50   Input ~ 0
~BUSACK
$Comp
L power:+5V #PWR?
U 1 1 5EF12760
P 8275 3325
AR Path="/5EF1043C/5EF12760" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5EF12760" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5EF12760" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8275 3175 50  0001 C CNN
F 1 "+5V" H 8290 3498 50  0000 C CNN
F 2 "" H 8275 3325 50  0001 C CNN
F 3 "" H 8275 3325 50  0001 C CNN
	1    8275 3325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5EF1276E
P 8275 3425
AR Path="/5EF1043C/5EF1276E" Ref="R9"  Part="1" 
AR Path="/5EFDB5C5/5EF1276E" Ref="R?"  Part="1" 
AR Path="/5EFDD352/5EF1276E" Ref="R?"  Part="1" 
F 0 "R9" H 8334 3471 50  0000 L CNN
F 1 "1k" H 8334 3380 50  0000 L CNN
F 2 "" H 8275 3425 50  0001 C CNN
F 3 "~" H 8275 3425 50  0001 C CNN
	1    8275 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8275 3725 8850 3725
Wire Wire Line
	8275 3525 8275 3725
$Comp
L Device:R_Small R5
U 1 1 5F0A3B92
P 5800 1125
F 0 "R5" H 5859 1171 50  0000 L CNN
F 1 "4k7" H 5859 1080 50  0000 L CNN
F 2 "" H 5800 1125 50  0001 C CNN
F 3 "~" H 5800 1125 50  0001 C CNN
	1    5800 1125
	1    0    0    -1  
$EndComp
NoConn ~ 7475 1575
Wire Wire Line
	6175 1775 6275 1775
$Comp
L 74xx:74LS122 U14
U 1 1 5EF326E9
P 6875 1775
F 0 "U14" H 6525 2450 50  0000 C CNN
F 1 "74LS122" H 6575 2350 50  0000 C CNN
F 2 "" H 6875 1775 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS122" H 6875 1775 50  0001 C CNN
	1    6875 1775
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF347AC
P 6875 1075
AR Path="/5EF2274D/5EF347AC" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF347AC" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF347AC" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5EF347AC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6875 925 50  0001 C CNN
F 1 "+5V" H 6890 1248 50  0000 C CNN
F 2 "" H 6875 1075 50  0001 C CNN
F 3 "" H 6875 1075 50  0001 C CNN
	1    6875 1075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF3510C
P 6875 2475
AR Path="/5EF2274D/5EF3510C" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF3510C" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF3510C" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5EF3510C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6875 2225 50  0001 C CNN
F 1 "GND" H 6880 2302 50  0000 C CNN
F 2 "" H 6875 2475 50  0001 C CNN
F 3 "" H 6875 2475 50  0001 C CNN
	1    6875 2475
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 5EFAB3A9
P 5800 2200
F 0 "C3" H 5888 2246 50  0000 L CNN
F 1 "6u8/16" H 5888 2155 50  0000 L CNN
F 2 "" H 5800 2200 50  0001 C CNN
F 3 "~" H 5800 2200 50  0001 C CNN
	1    5800 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFABB19
P 5800 2300
AR Path="/5EF2274D/5EFABB19" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EFABB19" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFABB19" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5EFABB19" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5800 2050 50  0001 C CNN
F 1 "GND" H 5805 2127 50  0000 C CNN
F 2 "" H 5800 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0001 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5EFAC115
P 5800 1925
F 0 "R4" H 5859 1971 50  0000 L CNN
F 1 "4k7" H 5859 1880 50  0000 L CNN
F 2 "" H 5800 1925 50  0001 C CNN
F 3 "~" H 5800 1925 50  0001 C CNN
	1    5800 1925
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EFAC5DF
P 5800 1825
AR Path="/5EF2274D/5EFAC5DF" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EFAC5DF" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFAC5DF" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5EFAC5DF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5800 1675 50  0001 C CNN
F 1 "+5V" H 5815 1998 50  0000 C CNN
F 2 "" H 5800 1825 50  0001 C CNN
F 3 "" H 5800 1825 50  0001 C CNN
	1    5800 1825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFAD9FE
P 6175 2325
AR Path="/5EF2274D/5EFAD9FE" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EFAD9FE" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFAD9FE" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5EFAD9FE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6175 2075 50  0001 C CNN
F 1 "GND" H 6180 2152 50  0000 C CNN
F 2 "" H 6175 2325 50  0001 C CNN
F 3 "" H 6175 2325 50  0001 C CNN
	1    6175 2325
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2075 5800 2100
Wire Wire Line
	6275 1875 6175 1875
Text Label 7775 1975 2    50   ~ 0
~RESET
NoConn ~ 6275 1475
$Comp
L power:+5V #PWR?
U 1 1 5F0A3B9F
P 5800 1025
AR Path="/5EF2274D/5F0A3B9F" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5F0A3B9F" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5F0A3B9F" Ref="#PWR?"  Part="1" 
AR Path="/5EF1043C/5F0A3B9F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5800 875 50  0001 C CNN
F 1 "+5V" H 5815 1198 50  0000 C CNN
F 2 "" H 5800 1025 50  0001 C CNN
F 3 "" H 5800 1025 50  0001 C CNN
	1    5800 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1225 5800 1275
Wire Wire Line
	6175 1275 6175 1375
Wire Wire Line
	5800 2025 5800 2075
Wire Wire Line
	5800 1300 5800 1275
$Comp
L Device:C_Small C4
U 1 1 5F0A26C9
P 5800 1400
F 0 "C4" H 5892 1446 50  0000 L CNN
F 1 "10n" H 5892 1355 50  0000 L CNN
F 2 "" H 5800 1400 50  0001 C CNN
F 3 "~" H 5800 1400 50  0001 C CNN
	1    5800 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6175 1875 6175 2325
Wire Wire Line
	6175 1875 6175 1775
Wire Wire Line
	6275 2075 5800 2075
Connection ~ 5800 2075
NoConn ~ 6275 1975
NoConn ~ 6275 2175
Connection ~ 6175 1875
Wire Wire Line
	6175 1375 6275 1375
Wire Wire Line
	5800 1275 6175 1275
Wire Wire Line
	6275 1575 5800 1575
Wire Wire Line
	5800 1575 5800 1500
Connection ~ 5800 1275
$Comp
L power:+5V #PWR?
U 1 1 5F877098
P 8000 3325
AR Path="/5EF1043C/5F877098" Ref="#PWR?"  Part="1" 
AR Path="/5EFDB5C5/5F877098" Ref="#PWR?"  Part="1" 
AR Path="/5EFDD352/5F877098" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8000 3175 50  0001 C CNN
F 1 "+5V" H 8015 3498 50  0000 C CNN
F 2 "" H 8000 3325 50  0001 C CNN
F 3 "" H 8000 3325 50  0001 C CNN
	1    8000 3325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5F8770A6
P 8000 3425
AR Path="/5EF1043C/5F8770A6" Ref="R7"  Part="1" 
AR Path="/5EFDB5C5/5F8770A6" Ref="R?"  Part="1" 
AR Path="/5EFDD352/5F8770A6" Ref="R?"  Part="1" 
F 0 "R7" H 8059 3471 50  0000 L CNN
F 1 "1k" H 8059 3380 50  0000 L CNN
F 2 "" H 8000 3425 50  0001 C CNN
F 3 "~" H 8000 3425 50  0001 C CNN
	1    8000 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3525 8000 3825
Wire Wire Line
	8000 3825 8850 3825
$Comp
L Device:Crystal Y1
U 1 1 5EF49378
P 2675 4000
F 0 "Y1" H 2675 4268 50  0000 C CNN
F 1 "4M" H 2675 4177 50  0000 C CNN
F 2 "" H 2675 4000 50  0001 C CNN
F 3 "~" H 2675 4000 50  0001 C CNN
	1    2675 4000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 2 1 5EF255F4
P 4025 4000
F 0 "U1" H 4025 4317 50  0000 C CNN
F 1 "74LS04" H 4025 4226 50  0000 C CNN
F 2 "" H 4025 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 4025 4000 50  0001 C CNN
	2    4025 4000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 3 1 5EF25BF9
P 6325 5025
F 0 "U1" H 6325 5342 50  0000 C CNN
F 1 "74LS04" H 6325 5251 50  0000 C CNN
F 2 "" H 6325 5025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6325 5025 50  0001 C CNN
	3    6325 5025
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 4 1 5EF260FA
P 6350 5500
F 0 "U1" H 6350 5817 50  0000 C CNN
F 1 "74LS04" H 6350 5726 50  0000 C CNN
F 2 "" H 6350 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6350 5500 50  0001 C CNN
	4    6350 5500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 5 1 5EF26661
P 3325 4000
F 0 "U1" H 3325 4317 50  0000 C CNN
F 1 "74LS04" H 3325 4226 50  0000 C CNN
F 2 "" H 3325 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 3325 4000 50  0001 C CNN
	5    3325 4000
	1    0    0    -1  
$EndComp
Connection ~ 2250 4000
$Comp
L 74xx:74LS04 U1
U 6 1 5EF27175
P 1925 4000
F 0 "U1" H 1925 4317 50  0000 C CNN
F 1 "74LS04" H 1925 4226 50  0000 C CNN
F 2 "" H 1925 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1925 4000 50  0001 C CNN
	6    1925 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5EF2C226
P 1900 4375
F 0 "R1" V 1704 4375 50  0000 C CNN
F 1 "1k" V 1795 4375 50  0000 C CNN
F 2 "" H 1900 4375 50  0001 C CNN
F 3 "~" H 1900 4375 50  0001 C CNN
	1    1900 4375
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF2C81A
P 2625 4525
F 0 "C?" V 2396 4525 50  0000 C CNN
F 1 "C_Small" V 2487 4525 50  0000 C CNN
F 2 "" H 2625 4525 50  0001 C CNN
F 3 "~" H 2625 4525 50  0001 C CNN
	1    2625 4525
	0    1    1    0   
$EndComp
Wire Wire Line
	3025 4000 3000 4000
Wire Wire Line
	2225 4000 2250 4000
Wire Wire Line
	2000 4375 2250 4375
Wire Wire Line
	2250 4375 2250 4000
Connection ~ 3650 4375
Wire Wire Line
	2250 4000 2525 4000
Wire Wire Line
	1800 4375 1600 4375
Wire Wire Line
	1600 4375 1600 4000
Wire Wire Line
	1600 4000 1625 4000
Wire Wire Line
	3650 4375 3650 4000
$Comp
L Device:R_Small R2
U 1 1 5EF32BEB
P 3300 4375
F 0 "R2" V 3104 4375 50  0000 C CNN
F 1 "1k" V 3195 4375 50  0000 C CNN
F 2 "" H 3300 4375 50  0001 C CNN
F 3 "~" H 3300 4375 50  0001 C CNN
	1    3300 4375
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4375 3650 4375
Wire Wire Line
	3000 4375 3000 4000
Wire Wire Line
	3200 4375 3000 4375
Connection ~ 3000 4000
Wire Wire Line
	3000 4000 2825 4000
Wire Wire Line
	3625 4000 3650 4000
Wire Wire Line
	2725 4525 3650 4525
Wire Wire Line
	3650 4525 3650 4375
Wire Wire Line
	2525 4525 1600 4525
Wire Wire Line
	1600 4525 1600 4375
Connection ~ 1600 4375
Wire Wire Line
	6025 5025 5975 5025
Wire Wire Line
	5975 5025 5975 5500
Wire Wire Line
	5975 5500 6050 5500
Wire Wire Line
	3725 4000 3650 4000
Connection ~ 3650 4000
$Comp
L 74xx:74LS74 U2
U 1 1 5EF3BC24
P 5950 3900
F 0 "U2" H 5950 4381 50  0000 C CNN
F 1 "74LS74" H 5950 4290 50  0000 C CNN
F 2 "" H 5950 3900 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 5950 3900 50  0001 C CNN
	1    5950 3900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U2
U 2 1 5EF3CB85
P 4900 4000
F 0 "U2" H 4900 4481 50  0000 C CNN
F 1 "74LS74" H 4900 4390 50  0000 C CNN
F 2 "" H 4900 4000 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 4900 4000 50  0001 C CNN
	2    4900 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4000 4325 4000
Wire Wire Line
	5200 4100 5325 4100
Wire Wire Line
	5325 4100 5325 3450
Wire Wire Line
	5325 3450 4550 3450
Wire Wire Line
	4550 3450 4550 3900
Wire Wire Line
	4550 3900 4600 3900
Wire Wire Line
	5650 3900 5525 3900
Wire Wire Line
	5650 3800 5600 3800
Wire Wire Line
	5600 3800 5600 3350
Wire Wire Line
	5600 3350 6375 3350
Wire Wire Line
	6375 3350 6375 4000
Wire Wire Line
	6375 4000 6250 4000
NoConn ~ 5950 4200
NoConn ~ 5950 3600
NoConn ~ 4900 3700
NoConn ~ 4900 4300
Wire Wire Line
	6250 3800 6525 3800
Connection ~ 5525 3900
Wire Wire Line
	5525 3900 5200 3900
$Comp
L Jumper:Jumper_3_Bridged12 JP?
U 1 1 5EF50756
P 5975 4550
F 0 "JP?" H 5975 4754 50  0000 C CNN
F 1 "1/2MHz" H 5975 4663 50  0000 C CNN
F 2 "" H 5975 4550 50  0001 C CNN
F 3 "~" H 5975 4550 50  0001 C CNN
	1    5975 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 4700 5975 5025
Connection ~ 5975 5025
Wire Wire Line
	5725 4550 5525 4550
Wire Wire Line
	5525 4550 5525 3900
Wire Wire Line
	6225 4550 6525 4550
Wire Wire Line
	6525 4550 6525 3800
Wire Wire Line
	6625 5025 7150 5025
Text Label 7150 5025 2    50   ~ 0
~PER_CLK
$Comp
L Device:R_Small R3
U 1 1 5EF63097
P 6725 5325
F 0 "R3" H 6784 5371 50  0000 L CNN
F 1 "330E" H 6784 5280 50  0000 L CNN
F 2 "" H 6725 5325 50  0001 C CNN
F 3 "~" H 6725 5325 50  0001 C CNN
	1    6725 5325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF6373C
P 6725 5225
F 0 "#PWR?" H 6725 5075 50  0001 C CNN
F 1 "+5V" H 6725 5375 50  0000 C CNN
F 2 "" H 6725 5225 50  0001 C CNN
F 3 "" H 6725 5225 50  0001 C CNN
	1    6725 5225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5500 6725 5500
Wire Wire Line
	6725 5500 6725 5425
Connection ~ 6725 5500
Text Label 7175 5500 2    50   ~ 0
~CPU_CLK
Wire Wire Line
	1250 2475 1650 2475
Text Label 1650 2475 2    50   ~ 0
~CPU_CLK
Text HLabel 1250 2475 0    50   Output ~ 0
~CPU_CLK
Text Label 1650 2600 2    50   ~ 0
~PER_CLK
Wire Wire Line
	1650 2600 1250 2600
Text HLabel 1250 2600 0    50   Output ~ 0
~PER_CLK
Text Label 8475 3425 0    50   ~ 0
~CPU_CLK
Wire Wire Line
	8525 1975 8525 3125
Wire Wire Line
	8525 3125 8850 3125
Wire Wire Line
	7475 1975 8525 1975
Wire Wire Line
	7825 5500 7825 3075
Wire Wire Line
	7825 3075 8450 3075
Wire Wire Line
	8450 3075 8450 3425
Wire Wire Line
	8450 3425 8850 3425
Wire Wire Line
	6725 5500 7825 5500
$EndSCHEMATC
