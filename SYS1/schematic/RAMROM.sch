EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Wire Wire Line
	6625 6025 6900 6025
Wire Wire Line
	6625 6425 6900 6425
Wire Wire Line
	6625 5925 6900 5925
Wire Wire Line
	6625 6525 6900 6525
Wire Wire Line
	6625 5825 6900 5825
Wire Wire Line
	6625 6325 6900 6325
Wire Wire Line
	6625 6225 6900 6225
Wire Wire Line
	6625 6125 6900 6125
$Comp
L power:+5V #PWR?
U 1 1 5F0166A8
P 6225 5625
AR Path="/5EF2274D/5F0166A8" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5F0166A8" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5F0166A8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6225 5475 50  0001 C CNN
F 1 "+5V" H 6240 5798 50  0000 C CNN
F 2 "" H 6225 5625 50  0001 C CNN
F 3 "" H 6225 5625 50  0001 C CNN
	1    6225 5625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F016DD7
P 6225 7525
AR Path="/5EF2274D/5F016DD7" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5F016DD7" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5F016DD7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6225 7275 50  0001 C CNN
F 1 "GND" H 6230 7352 50  0000 C CNN
F 2 "" H 6225 7525 50  0001 C CNN
F 3 "" H 6225 7525 50  0001 C CNN
	1    6225 7525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5825 5825 5525 5825
Wire Wire Line
	5825 5925 5525 5925
Wire Wire Line
	5825 6025 5525 6025
Wire Wire Line
	5825 6125 5525 6125
Wire Wire Line
	5825 6225 5525 6225
Wire Wire Line
	5825 6325 5525 6325
Wire Wire Line
	5825 6425 5525 6425
Wire Wire Line
	5825 6525 5525 6525
Wire Wire Line
	5825 6625 5525 6625
Wire Wire Line
	5825 6725 5525 6725
Wire Wire Line
	5825 6825 5525 6825
Wire Wire Line
	5825 6925 5525 6925
Wire Wire Line
	5825 7075 5525 7075
Text Label 5525 6725 0    50   ~ 0
A9
Text Label 5525 6825 0    50   ~ 0
A10
Text Label 5525 6925 0    50   ~ 0
A11
Wire Wire Line
	1200 1075 875  1075
Wire Wire Line
	1200 1875 875  1875
Wire Wire Line
	1200 1575 875  1575
Wire Wire Line
	1200 1275 875  1275
Wire Wire Line
	1200 1475 875  1475
Wire Wire Line
	1200 1775 875  1775
Wire Wire Line
	1200 2075 875  2075
Wire Wire Line
	1200 975  875  975 
Wire Wire Line
	1200 875  875  875 
Wire Wire Line
	1200 1175 875  1175
Text Label 1200 1575 2    50   ~ 0
~RD
Text Label 1200 1475 2    50   ~ 0
~HALT
Text Label 1200 1775 2    50   ~ 0
~MREQ
Text Label 1200 1875 2    50   ~ 0
~IORQ
Text Label 1200 1175 2    50   ~ 0
~M1
Text Label 1200 1975 2    50   ~ 0
~BUSRQ
Text Label 1200 975  2    50   ~ 0
~NMI
Text Label 1200 1375 2    50   ~ 0
~WAIT
Text Label 1200 875  2    50   ~ 0
~CLK
Text Label 1200 2075 2    50   ~ 0
~BUSACK
Text Label 1200 1275 2    50   ~ 0
~RFSH
Text Label 1200 1075 2    50   ~ 0
~INT
Wire Wire Line
	1200 775  875  775 
Wire Wire Line
	1200 1675 875  1675
Text Label 1200 1675 2    50   ~ 0
~WR
Text Label 1200 775  2    50   ~ 0
~RESET
Wire Wire Line
	1200 1375 875  1375
Wire Wire Line
	1200 1975 875  1975
Wire Wire Line
	1625 2000 1900 2000
Wire Wire Line
	1625 900  1900 900 
Wire Wire Line
	2200 1500 2475 1500
Wire Wire Line
	2200 1800 2475 1800
Wire Wire Line
	1625 1900 1900 1900
Wire Wire Line
	1625 600  1900 600 
Wire Wire Line
	1625 1300 1900 1300
Wire Wire Line
	1625 1400 1900 1400
Wire Wire Line
	1625 700  1900 700 
Wire Wire Line
	1625 1000 1900 1000
Wire Wire Line
	1625 1700 1900 1700
Wire Wire Line
	1625 1200 1900 1200
Wire Wire Line
	2200 1600 2475 1600
Wire Wire Line
	1625 1500 1900 1500
Text Label 1900 1000 2    50   ~ 0
A4
Wire Wire Line
	2200 2000 2475 2000
Text Label 1900 1100 2    50   ~ 0
A5
Wire Wire Line
	1625 1800 1900 1800
Wire Wire Line
	2200 1400 2475 1400
Wire Wire Line
	2200 1700 2475 1700
Wire Wire Line
	1625 800  1900 800 
Wire Wire Line
	1625 1600 1900 1600
Text Label 2475 1600 2    50   ~ 0
D2
Wire Wire Line
	2200 2100 2475 2100
Text Label 1900 1700 2    50   ~ 0
A11
Text Label 2475 1500 2    50   ~ 0
D1
Text Label 2475 1400 2    50   ~ 0
D0
Text Label 1900 1400 2    50   ~ 0
A8
Text Label 1900 1200 2    50   ~ 0
A6
Text Label 1900 1300 2    50   ~ 0
A7
Wire Wire Line
	1625 2100 1900 2100
Text Label 2475 1700 2    50   ~ 0
D3
Text Label 1900 1600 2    50   ~ 0
A10
Text Label 1900 2100 2    50   ~ 0
A15
Text Label 2475 1800 2    50   ~ 0
D4
Text Label 2475 2100 2    50   ~ 0
D7
Text Label 2475 1900 2    50   ~ 0
D5
Text Label 2475 2000 2    50   ~ 0
D6
Text Label 1900 1900 2    50   ~ 0
A13
Text Label 1900 2000 2    50   ~ 0
A14
Text Label 1900 800  2    50   ~ 0
A2
Text Label 1900 600  2    50   ~ 0
A0
Text Label 1900 900  2    50   ~ 0
A3
Text Label 1900 1800 2    50   ~ 0
A12
Text Label 1900 700  2    50   ~ 0
A1
Wire Wire Line
	1625 1100 1900 1100
Text Label 1900 1500 2    50   ~ 0
A9
Wire Wire Line
	2200 1900 2475 1900
Text HLabel 2200 1400 0    50   Input ~ 0
D0
Text HLabel 2200 1500 0    50   Input ~ 0
D1
Text HLabel 2200 1600 0    50   Input ~ 0
D2
Text HLabel 2200 1700 0    50   Input ~ 0
D3
Text HLabel 2200 1800 0    50   Input ~ 0
D4
Text HLabel 2200 1900 0    50   Input ~ 0
D5
Text HLabel 2200 2000 0    50   Input ~ 0
D6
Text HLabel 2200 2100 0    50   Input ~ 0
D7
Text HLabel 1625 600  0    50   Input ~ 0
A0
Text HLabel 1625 700  0    50   Input ~ 0
A1
Text HLabel 1625 800  0    50   Input ~ 0
A2
Text HLabel 1625 900  0    50   Input ~ 0
A3
Text HLabel 1625 1000 0    50   Input ~ 0
A4
Text HLabel 1625 1100 0    50   Input ~ 0
A5
Text HLabel 1625 1200 0    50   Input ~ 0
A6
Text HLabel 1625 1300 0    50   Input ~ 0
A7
Text HLabel 1625 1400 0    50   Input ~ 0
A8
Text HLabel 1625 1500 0    50   Input ~ 0
A9
Text HLabel 1625 1600 0    50   Input ~ 0
A10
Text HLabel 1625 1700 0    50   Input ~ 0
A11
Text HLabel 1625 1800 0    50   Input ~ 0
A12
Text HLabel 1625 1900 0    50   Input ~ 0
A13
Text HLabel 1625 2000 0    50   Input ~ 0
A14
Text HLabel 1625 2100 0    50   Input ~ 0
A15
Text HLabel 875  775  0    50   Input ~ 0
~RESET
Text HLabel 875  875  0    50   Input ~ 0
~CLK
Text HLabel 875  975  0    50   Input ~ 0
~NMI
Text HLabel 875  1075 0    50   Input ~ 0
~INT
Text HLabel 875  1175 0    50   Input ~ 0
~M1
Text HLabel 875  1275 0    50   Input ~ 0
~RFSH
Text HLabel 875  1375 0    50   Input ~ 0
~WAIT
Text HLabel 875  1475 0    50   Input ~ 0
~HALT
Text HLabel 875  1575 0    50   Input ~ 0
~RD
Text HLabel 875  1675 0    50   Input ~ 0
~WR
Text HLabel 875  1775 0    50   Input ~ 0
~MREQ
Text HLabel 875  1875 0    50   Input ~ 0
~IORQ
Text HLabel 875  1975 0    50   Input ~ 0
~BUSRQ
Text HLabel 875  2075 0    50   Input ~ 0
~BUSACK
$Comp
L my_ics:27C32 U15
U 1 1 5EF26201
P 6225 6625
F 0 "U15" H 5900 7675 50  0000 C CNN
F 1 "27C32" H 5975 7600 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 6225 6625 50  0001 C CNN
F 3 "" H 6225 6625 50  0001 C CNN
	1    6225 6625
	1    0    0    -1  
$EndComp
Text Label 5525 6325 0    50   ~ 0
A5
Text Label 5525 6025 0    50   ~ 0
A2
Text Label 5525 6125 0    50   ~ 0
A3
Text Label 5525 5925 0    50   ~ 0
A1
Text Label 5525 6425 0    50   ~ 0
A6
Text Label 5525 6525 0    50   ~ 0
A7
Text Label 5525 6625 0    50   ~ 0
A8
Text Label 5525 6225 0    50   ~ 0
A4
Text Label 5525 5825 0    50   ~ 0
A0
$Comp
L my_ics:RAM U24
U 1 1 5EF30F2E
P 10525 1325
F 0 "U24" H 10225 1950 50  0000 C CNN
F 1 "MK4564" H 10275 1875 50  0000 C CNN
F 2 "" H 10175 1325 50  0001 C CNN
F 3 "" H 10175 1325 50  0001 C CNN
	1    10525 1325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF31A19
P 10525 725
AR Path="/5EF2274D/5EF31A19" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF31A19" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF31A19" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10525 575 50  0001 C CNN
F 1 "+5V" H 10525 875 50  0000 C CNN
F 2 "" H 10525 725 50  0001 C CNN
F 3 "" H 10525 725 50  0001 C CNN
	1    10525 725 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF32E61
P 10875 1725
AR Path="/5EF2274D/5EF32E61" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF32E61" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF32E61" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10875 1475 50  0001 C CNN
F 1 "GND" H 10875 1575 50  0000 C CNN
F 2 "" H 10875 1725 50  0001 C CNN
F 3 "" H 10875 1725 50  0001 C CNN
	1    10875 1725
	1    0    0    -1  
$EndComp
Text Label 6900 6025 2    50   ~ 0
D2
Text Label 6900 5925 2    50   ~ 0
D1
Text Label 6900 5825 2    50   ~ 0
D0
Text Label 6900 6125 2    50   ~ 0
D3
Text Label 6900 6225 2    50   ~ 0
D4
Text Label 6900 6525 2    50   ~ 0
D7
Text Label 6900 6325 2    50   ~ 0
D5
Text Label 6900 6425 2    50   ~ 0
D6
NoConn ~ 10875 1225
Wire Wire Line
	10175 925  9975 925 
Wire Wire Line
	10175 1025 9975 1025
Wire Wire Line
	10175 1125 9975 1125
Wire Wire Line
	10175 1225 9975 1225
Wire Wire Line
	10175 1325 9975 1325
Wire Wire Line
	10175 1425 9975 1425
Wire Wire Line
	10175 1525 9975 1525
Wire Wire Line
	10175 1625 9975 1625
Wire Wire Line
	10175 1725 9975 1725
Text Label 9975 1025 0    50   ~ 0
AA0
Text Label 9975 1125 0    50   ~ 0
AA1
Text Label 9975 1225 0    50   ~ 0
AA2
Text Label 9975 1325 0    50   ~ 0
AA3
Text Label 9975 1425 0    50   ~ 0
AA4
Text Label 9975 1525 0    50   ~ 0
AA5
Text Label 9975 1625 0    50   ~ 0
AA6
Text Label 9975 1725 0    50   ~ 0
AA7
Wire Wire Line
	11075 925  10875 925 
Wire Wire Line
	11075 1025 10875 1025
Wire Wire Line
	11075 1125 10875 1125
Wire Wire Line
	11075 1325 10875 1325
Text Label 11075 1025 2    50   ~ 0
~RAS
Text Label 11075 1125 2    50   ~ 0
~CAS
Text Label 11075 1325 2    50   ~ 0
~WE
$Comp
L 74xx:74LS244 U20
U 1 1 5EF3736B
P 7850 3100
F 0 "U20" H 7500 3850 50  0000 C CNN
F 1 "74LS244" H 7600 3775 50  0000 C CNN
F 2 "" H 7850 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 7850 3100 50  0001 C CNN
	1    7850 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF39D75
P 7850 2300
AR Path="/5EF2274D/5EF39D75" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF39D75" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF39D75" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7850 2150 50  0001 C CNN
F 1 "+5V" H 7850 2450 50  0000 C CNN
F 2 "" H 7850 2300 50  0001 C CNN
F 3 "" H 7850 2300 50  0001 C CNN
	1    7850 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF3A78C
P 7850 3900
AR Path="/5EF2274D/5EF3A78C" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF3A78C" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF3A78C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7850 3650 50  0001 C CNN
F 1 "GND" H 7850 3750 50  0000 C CNN
F 2 "" H 7850 3900 50  0001 C CNN
F 3 "" H 7850 3900 50  0001 C CNN
	1    7850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2600 8500 2600
Wire Wire Line
	8350 2700 8500 2700
Wire Wire Line
	8350 2800 8500 2800
Wire Wire Line
	8350 2900 8500 2900
Wire Wire Line
	8350 3000 8500 3000
Wire Wire Line
	8350 3100 8500 3100
Wire Wire Line
	8350 3200 8500 3200
Wire Wire Line
	8350 3300 8500 3300
Wire Wire Line
	7200 2800 7350 2800
Wire Wire Line
	7200 3100 7350 3100
Wire Wire Line
	7200 3200 7350 3200
Wire Wire Line
	7200 3000 7350 3000
Wire Wire Line
	7200 2700 7350 2700
Wire Wire Line
	7200 2600 7350 2600
Wire Wire Line
	7200 2900 7350 2900
Wire Wire Line
	7200 3300 7350 3300
Wire Wire Line
	8550 1175 8675 1175
Wire Wire Line
	7500 1975 7525 1975
Wire Wire Line
	7375 975  7550 975 
Wire Wire Line
	7375 1375 7550 1375
Wire Wire Line
	8550 1475 8675 1475
Wire Wire Line
	8550 1575 8675 1575
Wire Wire Line
	8550 1375 8675 1375
Wire Wire Line
	7375 1475 7550 1475
Wire Wire Line
	7375 1275 7550 1275
Wire Wire Line
	8550 1075 8675 1075
Wire Wire Line
	8550 975  8675 975 
Wire Wire Line
	7375 1075 7550 1075
Wire Wire Line
	7375 1675 7550 1675
Wire Wire Line
	8550 1275 8675 1275
Wire Wire Line
	8550 1675 8675 1675
Wire Wire Line
	7375 1575 7550 1575
$Comp
L power:GND #PWR?
U 1 1 5EF3B29B
P 8050 2275
AR Path="/5EF2274D/5EF3B29B" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF3B29B" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF3B29B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8050 2025 50  0001 C CNN
F 1 "GND" H 8050 2125 50  0000 C CNN
F 2 "" H 8050 2275 50  0001 C CNN
F 3 "" H 8050 2275 50  0001 C CNN
	1    8050 2275
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF3B2A8
P 8050 675
AR Path="/5EF2274D/5EF3B2A8" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF3B2A8" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF3B2A8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8050 525 50  0001 C CNN
F 1 "+5V" H 8050 825 50  0000 C CNN
F 2 "" H 8050 675 50  0001 C CNN
F 3 "" H 8050 675 50  0001 C CNN
	1    8050 675 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS244 U19
U 1 1 5EF3B2C8
P 8050 1475
F 0 "U19" H 7700 2225 50  0000 C CNN
F 1 "74LS244" H 7800 2150 50  0000 C CNN
F 2 "" H 8050 1475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 8050 1475 50  0001 C CNN
	1    8050 1475
	1    0    0    -1  
$EndComp
Wire Wire Line
	7375 1175 7550 1175
$Comp
L power:GND #PWR?
U 1 1 5EF3E08C
P 7325 3625
AR Path="/5EF2274D/5EF3E08C" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF3E08C" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF3E08C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7325 3375 50  0001 C CNN
F 1 "GND" H 7325 3475 50  0000 C CNN
F 2 "" H 7325 3625 50  0001 C CNN
F 3 "" H 7325 3625 50  0001 C CNN
	1    7325 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	7325 3625 7325 3600
Wire Wire Line
	7325 3600 7350 3600
Text Label 7200 2900 0    50   ~ 0
D2
Text Label 7200 3000 0    50   ~ 0
D1
Text Label 7200 3200 0    50   ~ 0
D0
Text Label 7200 2800 0    50   ~ 0
D3
Text Label 7200 2700 0    50   ~ 0
D4
Text Label 7200 3300 0    50   ~ 0
D7
Text Label 7200 2600 0    50   ~ 0
D5
Text Label 7200 3100 0    50   ~ 0
D6
Text Label 8675 975  2    50   ~ 0
D7
Text Label 8675 1375 2    50   ~ 0
D2
Text Label 8675 1275 2    50   ~ 0
D1
Text Label 8675 1575 2    50   ~ 0
D4
Text Label 8675 1175 2    50   ~ 0
D6
Text Label 8675 1675 2    50   ~ 0
D5
Text Label 8675 1075 2    50   ~ 0
D0
Text Label 8675 1475 2    50   ~ 0
D3
Wire Wire Line
	7525 1975 7525 1875
Connection ~ 7525 1975
Wire Wire Line
	7525 1975 7550 1975
Wire Wire Line
	7525 1875 7550 1875
Wire Wire Line
	7325 3600 7325 3500
Connection ~ 7325 3600
Wire Wire Line
	7325 3500 7350 3500
$Comp
L 74xx:74LS32 U13
U 1 1 5EF407EE
P 4825 5400
F 0 "U13" H 4850 5400 50  0000 C CNN
F 1 "74LS32" H 4825 5200 50  0000 C CNN
F 2 "" H 4825 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4825 5400 50  0001 C CNN
	1    4825 5400
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS32 U13
U 2 1 5EF41B30
P 7200 1975
F 0 "U13" H 7200 2300 50  0000 C CNN
F 1 "74LS32" H 7200 2209 50  0000 C CNN
F 2 "" H 7200 1975 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7200 1975 50  0001 C CNN
	2    7200 1975
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U13
U 3 1 5EF427BA
P 4275 7175
F 0 "U13" H 4300 7175 50  0000 C CNN
F 1 "74LS32" H 4450 7025 50  0000 C CNN
F 2 "" H 4275 7175 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4275 7175 50  0001 C CNN
	3    4275 7175
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS32 U13
U 4 1 5EF436D5
P 3300 6800
F 0 "U13" H 3325 6800 50  0000 C CNN
F 1 "74LS32" H 3300 7000 50  0000 C CNN
F 2 "" H 3300 6800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3300 6800 50  0001 C CNN
	4    3300 6800
	1    0    0    -1  
$EndComp
Text Label 8500 3200 2    50   ~ 0
Di0
Text Label 8500 3000 2    50   ~ 0
Di1
Text Label 8500 2900 2    50   ~ 0
Di2
Text Label 8500 2800 2    50   ~ 0
Di3
Text Label 8500 2700 2    50   ~ 0
Di4
Text Label 8500 2600 2    50   ~ 0
Di5
Text Label 8500 3100 2    50   ~ 0
Di6
Text Label 8500 3300 2    50   ~ 0
Di7
Text Label 9900 925  2    50   ~ 0
Do0
Text Label 9900 2175 2    50   ~ 0
Do1
Text Label 9900 3425 2    50   ~ 0
Do2
Text Label 9900 4675 2    50   ~ 0
Do3
Text Label 11075 925  2    50   ~ 0
Do4
Text Label 11075 2175 2    50   ~ 0
Do5
Text Label 11075 3425 2    50   ~ 0
Do6
Text Label 11075 4675 2    50   ~ 0
Do7
Text Label 9975 925  0    50   ~ 0
Di4
Text Label 8800 925  0    50   ~ 0
Di0
Text Label 9975 4675 0    50   ~ 0
Di7
Text Label 8800 3425 0    50   ~ 0
Di2
Text Label 9975 2175 0    50   ~ 0
Di5
Text Label 8800 2175 0    50   ~ 0
Di1
Text Label 9975 3425 0    50   ~ 0
Di6
Text Label 8800 4675 0    50   ~ 0
Di3
Text Label 7375 1675 0    50   ~ 0
Do5
Text Label 7375 1575 0    50   ~ 0
Do4
Text Label 7375 1275 0    50   ~ 0
Do1
Text Label 7375 1475 0    50   ~ 0
Do3
Text Label 7375 1375 0    50   ~ 0
Do2
Text Label 7375 1175 0    50   ~ 0
Do6
Text Label 7375 975  0    50   ~ 0
Do7
Text Label 7375 1075 0    50   ~ 0
Do0
Wire Wire Line
	11075 2175 10875 2175
Wire Wire Line
	10175 2475 9975 2475
Wire Wire Line
	10175 2675 9975 2675
Text Label 9975 2275 0    50   ~ 0
AA0
Text Label 9975 2475 0    50   ~ 0
AA2
NoConn ~ 10875 2475
Wire Wire Line
	10175 2775 9975 2775
Wire Wire Line
	10175 2375 9975 2375
Wire Wire Line
	10175 2575 9975 2575
Wire Wire Line
	10175 2175 9975 2175
Text Label 11075 2275 2    50   ~ 0
~RAS
Text Label 11075 2375 2    50   ~ 0
~CAS
Wire Wire Line
	11075 2375 10875 2375
Text Label 9975 2575 0    50   ~ 0
AA3
Text Label 9975 2775 0    50   ~ 0
AA5
Text Label 9975 2375 0    50   ~ 0
AA1
Wire Wire Line
	10175 2975 9975 2975
Wire Wire Line
	11075 2575 10875 2575
Wire Wire Line
	10175 2875 9975 2875
Text Label 11075 2575 2    50   ~ 0
~WE
Text Label 9975 2975 0    50   ~ 0
AA7
Text Label 9975 2675 0    50   ~ 0
AA4
Wire Wire Line
	11075 2275 10875 2275
Wire Wire Line
	10175 2275 9975 2275
Text Label 9975 2875 0    50   ~ 0
AA6
$Comp
L power:+5V #PWR?
U 1 1 5EF5A3E6
P 10525 1975
AR Path="/5EF2274D/5EF5A3E6" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5A3E6" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5A3E6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10525 1825 50  0001 C CNN
F 1 "+5V" H 10525 2125 50  0000 C CNN
F 2 "" H 10525 1975 50  0001 C CNN
F 3 "" H 10525 1975 50  0001 C CNN
	1    10525 1975
	1    0    0    -1  
$EndComp
$Comp
L my_ics:RAM U23
U 1 1 5EF5A402
P 10525 2575
F 0 "U23" H 10225 3200 50  0000 C CNN
F 1 "MK4564" H 10275 3125 50  0000 C CNN
F 2 "" H 10175 2575 50  0001 C CNN
F 3 "" H 10175 2575 50  0001 C CNN
	1    10525 2575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF5A40F
P 10875 2975
AR Path="/5EF2274D/5EF5A40F" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5A40F" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5A40F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10875 2725 50  0001 C CNN
F 1 "GND" H 10875 2825 50  0000 C CNN
F 2 "" H 10875 2975 50  0001 C CNN
F 3 "" H 10875 2975 50  0001 C CNN
	1    10875 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	11075 3425 10875 3425
Wire Wire Line
	10175 3725 9975 3725
Wire Wire Line
	10175 3925 9975 3925
Text Label 11075 5075 2    50   ~ 0
~WE
Text Label 9975 3525 0    50   ~ 0
AA0
Text Label 9975 3725 0    50   ~ 0
AA2
NoConn ~ 10875 3725
Wire Wire Line
	10175 4025 9975 4025
Wire Wire Line
	10175 3625 9975 3625
Text Label 9975 5275 0    50   ~ 0
AA5
Wire Wire Line
	10175 3825 9975 3825
Wire Wire Line
	10175 3425 9975 3425
Text Label 11075 3525 2    50   ~ 0
~RAS
Text Label 11075 3625 2    50   ~ 0
~CAS
Wire Wire Line
	11075 3625 10875 3625
Text Label 9975 5175 0    50   ~ 0
AA4
Text Label 9975 5375 0    50   ~ 0
AA6
Text Label 9975 3825 0    50   ~ 0
AA3
Text Label 9975 4025 0    50   ~ 0
AA5
Text Label 9975 3625 0    50   ~ 0
AA1
Text Label 9975 4775 0    50   ~ 0
AA0
Text Label 11075 4875 2    50   ~ 0
~CAS
$Comp
L power:+5V #PWR?
U 1 1 5EF5C6A4
P 10525 4475
AR Path="/5EF2274D/5EF5C6A4" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5C6A4" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5C6A4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10525 4325 50  0001 C CNN
F 1 "+5V" H 10525 4625 50  0000 C CNN
F 2 "" H 10525 4475 50  0001 C CNN
F 3 "" H 10525 4475 50  0001 C CNN
	1    10525 4475
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF5C6B1
P 10875 5475
AR Path="/5EF2274D/5EF5C6B1" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5C6B1" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5C6B1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10875 5225 50  0001 C CNN
F 1 "GND" H 10875 5325 50  0000 C CNN
F 2 "" H 10875 5475 50  0001 C CNN
F 3 "" H 10875 5475 50  0001 C CNN
	1    10875 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	10175 4225 9975 4225
Wire Wire Line
	10175 4675 9975 4675
Wire Wire Line
	11075 3825 10875 3825
Wire Wire Line
	10175 4125 9975 4125
Text Label 11075 3825 2    50   ~ 0
~WE
Text Label 9975 4225 0    50   ~ 0
AA7
Text Label 9975 3925 0    50   ~ 0
AA4
Wire Wire Line
	10175 5175 9975 5175
Wire Wire Line
	11075 3525 10875 3525
Text Label 11075 4775 2    50   ~ 0
~RAS
Wire Wire Line
	10175 4975 9975 4975
NoConn ~ 10875 4975
Text Label 9975 5075 0    50   ~ 0
AA3
Wire Wire Line
	10175 5275 9975 5275
Wire Wire Line
	10175 4875 9975 4875
Wire Wire Line
	11075 4675 10875 4675
Wire Wire Line
	10175 3525 9975 3525
$Comp
L my_ics:RAM U21
U 1 1 5EF5C6DE
P 10525 5075
F 0 "U21" H 10225 5700 50  0000 C CNN
F 1 "MK4564" H 10275 5625 50  0000 C CNN
F 2 "" H 10175 5075 50  0001 C CNN
F 3 "" H 10175 5075 50  0001 C CNN
	1    10525 5075
	1    0    0    -1  
$EndComp
Text Label 9975 4125 0    50   ~ 0
AA6
$Comp
L power:+5V #PWR?
U 1 1 5EF5C6EC
P 10525 3225
AR Path="/5EF2274D/5EF5C6EC" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5C6EC" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5C6EC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10525 3075 50  0001 C CNN
F 1 "+5V" H 10525 3375 50  0000 C CNN
F 2 "" H 10525 3225 50  0001 C CNN
F 3 "" H 10525 3225 50  0001 C CNN
	1    10525 3225
	1    0    0    -1  
$EndComp
$Comp
L my_ics:RAM U22
U 1 1 5EF5C708
P 10525 3825
F 0 "U22" H 10225 4450 50  0000 C CNN
F 1 "MK4564" H 10275 4375 50  0000 C CNN
F 2 "" H 10175 3825 50  0001 C CNN
F 3 "" H 10175 3825 50  0001 C CNN
	1    10525 3825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF5C715
P 10875 4225
AR Path="/5EF2274D/5EF5C715" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF5C715" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF5C715" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10875 3975 50  0001 C CNN
F 1 "GND" H 10875 4075 50  0000 C CNN
F 2 "" H 10875 4225 50  0001 C CNN
F 3 "" H 10875 4225 50  0001 C CNN
	1    10875 4225
	1    0    0    -1  
$EndComp
Text Label 9975 4975 0    50   ~ 0
AA2
Text Label 9975 4875 0    50   ~ 0
AA1
Text Label 9975 5475 0    50   ~ 0
AA7
Wire Wire Line
	10175 5075 9975 5075
Wire Wire Line
	10175 5375 9975 5375
Wire Wire Line
	11075 4875 10875 4875
Wire Wire Line
	11075 4775 10875 4775
Wire Wire Line
	10175 5475 9975 5475
Wire Wire Line
	10175 4775 9975 4775
Wire Wire Line
	11075 5075 10875 5075
Wire Wire Line
	9900 925  9700 925 
Wire Wire Line
	9000 1225 8800 1225
Wire Wire Line
	9900 3425 9700 3425
Wire Wire Line
	9000 1425 8800 1425
Wire Wire Line
	9900 4675 9700 4675
$Comp
L power:GND #PWR?
U 1 1 5EF61581
P 9700 5475
AR Path="/5EF2274D/5EF61581" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF61581" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF61581" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9700 5225 50  0001 C CNN
F 1 "GND" H 9700 5325 50  0000 C CNN
F 2 "" H 9700 5475 50  0001 C CNN
F 3 "" H 9700 5475 50  0001 C CNN
	1    9700 5475
	1    0    0    -1  
$EndComp
Text Label 9900 3525 2    50   ~ 0
~RAS
Wire Wire Line
	9900 3825 9700 3825
Text Label 8800 4025 0    50   ~ 0
AA5
Text Label 9900 5075 2    50   ~ 0
~WE
Text Label 8800 3725 0    50   ~ 0
AA2
Text Label 9900 2575 2    50   ~ 0
~WE
Text Label 8800 1025 0    50   ~ 0
AA0
Text Label 8800 5475 0    50   ~ 0
AA7
Text Label 8800 3625 0    50   ~ 0
AA1
Text Label 8800 5175 0    50   ~ 0
AA4
Text Label 9900 3825 2    50   ~ 0
~WE
Text Label 9900 4875 2    50   ~ 0
~CAS
Wire Wire Line
	9000 4675 8800 4675
Text Label 8800 1225 0    50   ~ 0
AA2
Wire Wire Line
	9900 4875 9700 4875
NoConn ~ 9700 1225
Text Label 8800 4875 0    50   ~ 0
AA1
Text Label 8800 4975 0    50   ~ 0
AA2
Wire Wire Line
	9000 1525 8800 1525
Wire Wire Line
	9000 1125 8800 1125
Text Label 8800 2775 0    50   ~ 0
AA5
Text Label 9900 3625 2    50   ~ 0
~CAS
Wire Wire Line
	9000 4025 8800 4025
Wire Wire Line
	9000 1325 8800 1325
Wire Wire Line
	9000 925  8800 925 
Wire Wire Line
	9000 5075 8800 5075
Wire Wire Line
	9000 3825 8800 3825
Text Label 9900 1025 2    50   ~ 0
~RAS
Text Label 9900 1125 2    50   ~ 0
~CAS
Wire Wire Line
	9900 1125 9700 1125
Wire Wire Line
	9000 5275 8800 5275
Wire Wire Line
	9000 4875 8800 4875
Wire Wire Line
	9000 3725 8800 3725
Text Label 8800 2675 0    50   ~ 0
AA4
Text Label 8800 2875 0    50   ~ 0
AA6
Text Label 8800 1325 0    50   ~ 0
AA3
Wire Wire Line
	9900 3525 9700 3525
Text Label 8800 1525 0    50   ~ 0
AA5
$Comp
L my_ics:RAM U25
U 1 1 5EF615C3
P 9350 5075
F 0 "U25" H 9050 5700 50  0000 C CNN
F 1 "MK4564" H 9100 5625 50  0000 C CNN
F 2 "" H 9000 5075 50  0001 C CNN
F 3 "" H 9000 5075 50  0001 C CNN
	1    9350 5075
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 4775 8800 4775
Wire Wire Line
	9900 4775 9700 4775
Wire Wire Line
	9000 3925 8800 3925
Text Label 8800 1125 0    50   ~ 0
AA1
Text Label 8800 2275 0    50   ~ 0
AA0
Text Label 9900 2375 2    50   ~ 0
~CAS
$Comp
L power:+5V #PWR?
U 1 1 5EF615D6
P 9350 1975
AR Path="/5EF2274D/5EF615D6" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF615D6" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF615D6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9350 1825 50  0001 C CNN
F 1 "+5V" H 9350 2125 50  0000 C CNN
F 2 "" H 9350 1975 50  0001 C CNN
F 3 "" H 9350 1975 50  0001 C CNN
	1    9350 1975
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF615E3
P 9700 2975
AR Path="/5EF2274D/5EF615E3" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF615E3" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF615E3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9700 2725 50  0001 C CNN
F 1 "GND" H 9700 2825 50  0000 C CNN
F 2 "" H 9700 2975 50  0001 C CNN
F 3 "" H 9700 2975 50  0001 C CNN
	1    9700 2975
	1    0    0    -1  
$EndComp
Text Label 8800 5375 0    50   ~ 0
AA6
Wire Wire Line
	9000 1725 8800 1725
Wire Wire Line
	9000 2175 8800 2175
Wire Wire Line
	9900 1325 9700 1325
Wire Wire Line
	9000 1625 8800 1625
Text Label 8800 3525 0    50   ~ 0
AA0
$Comp
L power:+5V #PWR?
U 1 1 5EF615F6
P 9350 4475
AR Path="/5EF2274D/5EF615F6" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF615F6" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF615F6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9350 4325 50  0001 C CNN
F 1 "+5V" H 9350 4625 50  0000 C CNN
F 2 "" H 9350 4475 50  0001 C CNN
F 3 "" H 9350 4475 50  0001 C CNN
	1    9350 4475
	1    0    0    -1  
$EndComp
Text Label 8800 4225 0    50   ~ 0
AA7
Text Label 9900 1325 2    50   ~ 0
~WE
Wire Wire Line
	9000 4125 8800 4125
Text Label 8800 1725 0    50   ~ 0
AA7
Text Label 8800 4125 0    50   ~ 0
AA6
$Comp
L power:+5V #PWR?
U 1 1 5EF61608
P 9350 3225
AR Path="/5EF2274D/5EF61608" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF61608" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF61608" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9350 3075 50  0001 C CNN
F 1 "+5V" H 9350 3375 50  0000 C CNN
F 2 "" H 9350 3225 50  0001 C CNN
F 3 "" H 9350 3225 50  0001 C CNN
	1    9350 3225
	1    0    0    -1  
$EndComp
Text Label 8800 3925 0    50   ~ 0
AA4
Text Label 9900 4775 2    50   ~ 0
~RAS
Text Label 8800 1425 0    50   ~ 0
AA4
Wire Wire Line
	9000 2675 8800 2675
Wire Wire Line
	9900 1025 9700 1025
Text Label 9900 2275 2    50   ~ 0
~RAS
Wire Wire Line
	9900 5075 9700 5075
Wire Wire Line
	9000 2475 8800 2475
NoConn ~ 9700 2475
Text Label 8800 2575 0    50   ~ 0
AA3
Wire Wire Line
	9000 4975 8800 4975
Wire Wire Line
	9000 5175 8800 5175
Wire Wire Line
	9000 5375 8800 5375
Wire Wire Line
	9000 4225 8800 4225
Wire Wire Line
	9000 2775 8800 2775
Wire Wire Line
	9000 3425 8800 3425
NoConn ~ 9700 4975
Wire Wire Line
	9000 2375 8800 2375
Wire Wire Line
	9900 2175 9700 2175
Wire Wire Line
	9900 3625 9700 3625
Text Label 8800 5275 0    50   ~ 0
AA5
Wire Wire Line
	9000 5475 8800 5475
Wire Wire Line
	9000 1025 8800 1025
$Comp
L my_ics:RAM U26
U 1 1 5EF6163B
P 9350 3825
F 0 "U26" H 9050 4450 50  0000 C CNN
F 1 "MK4564" H 9100 4375 50  0000 C CNN
F 2 "" H 9000 3825 50  0001 C CNN
F 3 "" H 9000 3825 50  0001 C CNN
	1    9350 3825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF61648
P 9700 4225
AR Path="/5EF2274D/5EF61648" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF61648" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF61648" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9700 3975 50  0001 C CNN
F 1 "GND" H 9700 4075 50  0000 C CNN
F 2 "" H 9700 4225 50  0001 C CNN
F 3 "" H 9700 4225 50  0001 C CNN
	1    9700 4225
	1    0    0    -1  
$EndComp
$Comp
L my_ics:RAM U27
U 1 1 5EF61664
P 9350 2575
F 0 "U27" H 9050 3200 50  0000 C CNN
F 1 "MK4564" H 9100 3125 50  0000 C CNN
F 2 "" H 9000 2575 50  0001 C CNN
F 3 "" H 9000 2575 50  0001 C CNN
	1    9350 2575
	1    0    0    -1  
$EndComp
Text Label 8800 1625 0    50   ~ 0
AA6
$Comp
L power:+5V #PWR?
U 1 1 5EF61672
P 9350 725
AR Path="/5EF2274D/5EF61672" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF61672" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF61672" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9350 575 50  0001 C CNN
F 1 "+5V" H 9350 875 50  0000 C CNN
F 2 "" H 9350 725 50  0001 C CNN
F 3 "" H 9350 725 50  0001 C CNN
	1    9350 725 
	1    0    0    -1  
$EndComp
$Comp
L my_ics:RAM U28
U 1 1 5EF6168E
P 9350 1325
F 0 "U28" H 9050 1950 50  0000 C CNN
F 1 "MK4564" H 9100 1875 50  0000 C CNN
F 2 "" H 9000 1325 50  0001 C CNN
F 3 "" H 9000 1325 50  0001 C CNN
	1    9350 1325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF6169B
P 9700 1725
AR Path="/5EF2274D/5EF6169B" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF6169B" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF6169B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9700 1475 50  0001 C CNN
F 1 "GND" H 9700 1575 50  0000 C CNN
F 2 "" H 9700 1725 50  0001 C CNN
F 3 "" H 9700 1725 50  0001 C CNN
	1    9700 1725
	1    0    0    -1  
$EndComp
Text Label 8800 4775 0    50   ~ 0
AA0
Text Label 8800 5075 0    50   ~ 0
AA3
Text Label 8800 3825 0    50   ~ 0
AA3
Text Label 8800 2475 0    50   ~ 0
AA2
Text Label 8800 2375 0    50   ~ 0
AA1
Wire Wire Line
	9000 3525 8800 3525
Wire Wire Line
	9000 3625 8800 3625
Text Label 8800 2975 0    50   ~ 0
AA7
Wire Wire Line
	9000 2575 8800 2575
Wire Wire Line
	9000 2875 8800 2875
Wire Wire Line
	9900 2375 9700 2375
Wire Wire Line
	9900 2275 9700 2275
Wire Wire Line
	9000 2975 8800 2975
Wire Wire Line
	9000 2275 8800 2275
Wire Wire Line
	9900 2575 9700 2575
NoConn ~ 9700 3725
$Comp
L 74xx:74LS157 U17
U 1 1 5EF7C43E
P 7900 5275
F 0 "U17" H 7525 6150 50  0000 C CNN
F 1 "74LS157" H 7600 6050 50  0000 C CNN
F 2 "" H 7900 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS157" H 7900 5275 50  0001 C CNN
	1    7900 5275
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF7EE64
P 7900 4375
AR Path="/5EF2274D/5EF7EE64" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF7EE64" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF7EE64" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7900 4225 50  0001 C CNN
F 1 "+5V" H 7915 4548 50  0000 C CNN
F 2 "" H 7900 4375 50  0001 C CNN
F 3 "" H 7900 4375 50  0001 C CNN
	1    7900 4375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF7F46A
P 7900 6275
AR Path="/5EF2274D/5EF7F46A" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF7F46A" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF7F46A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7900 6025 50  0001 C CNN
F 1 "GND" H 7905 6102 50  0000 C CNN
F 2 "" H 7900 6275 50  0001 C CNN
F 3 "" H 7900 6275 50  0001 C CNN
	1    7900 6275
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4675 7250 4675
Wire Wire Line
	7400 4775 7250 4775
Wire Wire Line
	7100 2600 6950 2600
Wire Wire Line
	7400 4975 7250 4975
Wire Wire Line
	7400 5075 7250 5075
Wire Wire Line
	7400 5275 7250 5275
Wire Wire Line
	7400 5375 7250 5375
Wire Wire Line
	7100 3200 6950 3200
Wire Wire Line
	7400 5575 7250 5575
Wire Wire Line
	7400 5675 7250 5675
Wire Wire Line
	7100 3500 6950 3500
Wire Wire Line
	7400 5875 7000 5875
Wire Wire Line
	7100 2900 6950 2900
Wire Wire Line
	5750 3300 5600 3300
Wire Wire Line
	5750 3200 5600 3200
$Comp
L power:GND #PWR?
U 1 1 5EF81279
P 6250 4200
AR Path="/5EF2274D/5EF81279" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF81279" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF81279" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6250 3950 50  0001 C CNN
F 1 "GND" H 6255 4027 50  0000 C CNN
F 2 "" H 6250 4200 50  0001 C CNN
F 3 "" H 6250 4200 50  0001 C CNN
	1    6250 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF81286
P 6250 2300
AR Path="/5EF2274D/5EF81286" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF81286" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF81286" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6250 2150 50  0001 C CNN
F 1 "+5V" H 6265 2473 50  0000 C CNN
F 2 "" H 6250 2300 50  0001 C CNN
F 3 "" H 6250 2300 50  0001 C CNN
	1    6250 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3800 5350 3800
Wire Wire Line
	5750 3900 5725 3900
Wire Wire Line
	5750 3500 5600 3500
Wire Wire Line
	5750 2900 5600 2900
Wire Wire Line
	5750 2600 5600 2600
$Comp
L 74xx:74LS157 U18
U 1 1 5EF812AB
P 6250 3200
F 0 "U18" H 5875 4075 50  0000 C CNN
F 1 "74LS157" H 5950 3975 50  0000 C CNN
F 2 "" H 6250 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS157" H 6250 3200 50  0001 C CNN
	1    6250 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3600 5600 3600
Wire Wire Line
	5750 2700 5600 2700
Wire Wire Line
	5750 3000 5600 3000
$Comp
L Device:R_Small R?
U 1 1 5EF86879
P 6850 2600
F 0 "R?" V 6654 2600 50  0000 C CNN
F 1 "33E" V 6745 2600 50  0000 C CNN
F 2 "" H 6850 2600 50  0001 C CNN
F 3 "~" H 6850 2600 50  0001 C CNN
	1    6850 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF8705B
P 6850 2900
F 0 "R?" V 6654 2900 50  0000 C CNN
F 1 "33E" V 6745 2900 50  0000 C CNN
F 2 "" H 6850 2900 50  0001 C CNN
F 3 "~" H 6850 2900 50  0001 C CNN
	1    6850 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF8749A
P 6850 3200
F 0 "R?" V 6654 3200 50  0000 C CNN
F 1 "33E" V 6745 3200 50  0000 C CNN
F 2 "" H 6850 3200 50  0001 C CNN
F 3 "~" H 6850 3200 50  0001 C CNN
	1    6850 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF87801
P 6850 3500
F 0 "R?" V 6654 3500 50  0000 C CNN
F 1 "33E" V 6745 3500 50  0000 C CNN
F 2 "" H 6850 3500 50  0001 C CNN
F 3 "~" H 6850 3500 50  0001 C CNN
	1    6850 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 5575 8600 5575
$Comp
L Device:R_Small R?
U 1 1 5EF895CE
P 8500 4975
F 0 "R?" V 8304 4975 50  0000 C CNN
F 1 "33E" V 8395 4975 50  0000 C CNN
F 2 "" H 8500 4975 50  0001 C CNN
F 3 "~" H 8500 4975 50  0001 C CNN
	1    8500 4975
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF895DC
P 8500 5575
F 0 "R?" V 8304 5575 50  0000 C CNN
F 1 "33E" V 8395 5575 50  0000 C CNN
F 2 "" H 8500 5575 50  0001 C CNN
F 3 "~" H 8500 5575 50  0001 C CNN
	1    8500 5575
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 5275 8600 5275
Wire Wire Line
	8750 4675 8600 4675
Wire Wire Line
	8750 4975 8600 4975
$Comp
L Device:R_Small R?
U 1 1 5EF895ED
P 8500 4675
F 0 "R?" V 8304 4675 50  0000 C CNN
F 1 "33E" V 8395 4675 50  0000 C CNN
F 2 "" H 8500 4675 50  0001 C CNN
F 3 "~" H 8500 4675 50  0001 C CNN
	1    8500 4675
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5EF895FB
P 8500 5275
F 0 "R?" V 8304 5275 50  0000 C CNN
F 1 "33E" V 8395 5275 50  0000 C CNN
F 2 "" H 8500 5275 50  0001 C CNN
F 3 "~" H 8500 5275 50  0001 C CNN
	1    8500 5275
	0    1    1    0   
$EndComp
Text Label 7100 2600 2    50   ~ 0
AA2
Text Label 8750 4675 2    50   ~ 0
AA5
Text Label 8750 5275 2    50   ~ 0
AA4
Text Label 7100 2900 2    50   ~ 0
AA1
Text Label 8750 4975 2    50   ~ 0
AA6
Text Label 8750 5575 2    50   ~ 0
AA7
Text Label 7100 3200 2    50   ~ 0
AA0
Text Label 7100 3500 2    50   ~ 0
AA3
Text Label 5600 3500 0    50   ~ 0
A11
Text Label 7250 4775 0    50   ~ 0
A5
Text Label 7250 5375 0    50   ~ 0
A4
Text Label 5600 3200 0    50   ~ 0
A8
Text Label 7250 5075 0    50   ~ 0
A6
Text Label 7250 5675 0    50   ~ 0
A7
Text Label 5600 2600 0    50   ~ 0
A10
Text Label 7250 5575 0    50   ~ 0
A15
Text Label 7250 4675 0    50   ~ 0
A13
Text Label 7250 4975 0    50   ~ 0
A14
Text Label 5600 2700 0    50   ~ 0
A2
Text Label 5600 3300 0    50   ~ 0
A0
Text Label 5600 3600 0    50   ~ 0
A3
Text Label 7250 5275 0    50   ~ 0
A12
Text Label 5600 3000 0    50   ~ 0
A1
Text Label 5600 2900 0    50   ~ 0
A9
$Comp
L power:GND #PWR?
U 1 1 5EF94CFF
P 5725 3925
AR Path="/5EF2274D/5EF94CFF" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF94CFF" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF94CFF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5725 3675 50  0001 C CNN
F 1 "GND" H 5730 3752 50  0000 C CNN
F 2 "" H 5725 3925 50  0001 C CNN
F 3 "" H 5725 3925 50  0001 C CNN
	1    5725 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5725 3925 5725 3900
Wire Wire Line
	7375 6000 7375 5975
$Comp
L power:GND #PWR?
U 1 1 5EF952D5
P 7375 6000
AR Path="/5EF2274D/5EF952D5" Ref="#PWR?"  Part="1" 
AR Path="/5EF239C3/5EF952D5" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF952D5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7375 5750 50  0001 C CNN
F 1 "GND" H 7380 5827 50  0000 C CNN
F 2 "" H 7375 6000 50  0001 C CNN
F 3 "" H 7375 6000 50  0001 C CNN
	1    7375 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5975 7375 5975
$Comp
L Device:R_Small R10
U 1 1 5EF99A5C
P 6125 1500
F 0 "R10" V 6075 1325 50  0000 C CNN
F 1 "33E" V 6075 1650 50  0000 C CNN
F 2 "" H 6125 1500 50  0001 C CNN
F 3 "~" H 6125 1500 50  0001 C CNN
	1    6125 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	6225 1500 6550 1500
Text Label 6550 1500 2    50   ~ 0
~RAS
$Comp
L Device:R_Small R12
U 1 1 5EFA463E
P 5500 5400
F 0 "R12" V 5425 5325 50  0000 C CNN
F 1 "33E" V 5425 5550 50  0000 C CNN
F 2 "" H 5500 5400 50  0001 C CNN
F 3 "~" H 5500 5400 50  0001 C CNN
	1    5500 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 5400 5900 5400
Text Label 5900 5400 2    50   ~ 0
~CAS
Wire Wire Line
	5400 5400 5275 5400
$Comp
L Device:R_Small R?
U 1 1 5EFB15DA
P 2675 675
F 0 "R?" V 2625 500 50  0000 C CNN
F 1 "33E" V 2625 825 50  0000 C CNN
F 2 "" H 2675 675 50  0001 C CNN
F 3 "~" H 2675 675 50  0001 C CNN
	1    2675 675 
	0    1    1    0   
$EndComp
Wire Wire Line
	2775 675  3100 675 
Text Label 3100 675  2    50   ~ 0
~WE
Text HLabel 2400 675  0    50   Input ~ 0
~RAM_WE
Wire Wire Line
	2400 675  2575 675 
$Comp
L 74xx:74LS32 U10
U 4 1 5F05C309
P 1175 6900
F 0 "U10" H 1200 6900 50  0000 C CNN
F 1 "74LS32" H 1225 7100 50  0000 C CNN
F 2 "" H 1175 6900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 1175 6900 50  0001 C CNN
	4    1175 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3975 7275 3550 7275
Text HLabel 2400 800  0    50   Input ~ 0
~MREQb
Wire Wire Line
	2400 800  2800 800 
Text Label 2800 800  2    50   ~ 0
~MREQb
Text Label 3550 7275 0    50   ~ 0
~MREQb
Wire Wire Line
	3700 6600 3625 6600
$Comp
L 74xx:74LS00 U9
U 2 1 5EFB9A86
P 2550 6900
F 0 "U9" H 2525 6900 50  0000 C CNN
F 1 "74LS00" H 2825 7025 50  0000 C CNN
F 2 "" H 2550 6900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2550 6900 50  0001 C CNN
	2    2550 6900
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 7575 2000 7575
Text Label 2000 7575 0    50   ~ 0
~RESET
$Comp
L 74xx:74LS00 U9
U 1 1 5EFB880C
P 2550 7475
F 0 "U9" H 2525 7475 50  0000 C CNN
F 1 "74LS00" H 2825 7600 50  0000 C CNN
F 2 "" H 2550 7475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 2550 7475 50  0001 C CNN
	1    2550 7475
	1    0    0    1   
$EndComp
Wire Wire Line
	6900 2075 6700 2075
Wire Wire Line
	2750 925  2350 925 
Text Label 2750 925  2    50   ~ 0
~RDb
Text HLabel 2350 925  0    50   Input ~ 0
~RDb
Text Label 6700 2075 0    50   ~ 0
~RDb
Text Label 5575 1500 0    50   ~ 0
~MREQb
Wire Wire Line
	6025 1500 5575 1500
Text Label 7000 5875 0    50   ~ 0
ROW{slash}COL
Text Label 5350 3800 0    50   ~ 0
ROW{slash}COL
Wire Wire Line
	4575 7175 5825 7175
Wire Wire Line
	3625 6600 3625 6800
Wire Wire Line
	3625 7075 3975 7075
Text Label 2775 6700 0    50   ~ 0
A15
Text Label 5525 7075 0    50   ~ 0
~RDb
$Comp
L my_ics:74LS367_sep U16
U 1 1 5F8860BE
P 2225 5575
F 0 "U16" H 2225 5800 50  0000 C CNN
F 1 "74LS367" H 2225 5709 50  0000 C CNN
F 2 "" H 2325 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 2325 5275 50  0001 C CNN
	1    2225 5575
	1    0    0    -1  
$EndComp
$Comp
L my_ics:74LS367_sep U16
U 6 1 5F8AAFA0
P 1025 5575
F 0 "U16" H 1025 5800 50  0000 C CNN
F 1 "74LS367" H 1025 5709 50  0000 C CNN
F 2 "" H 1125 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 1025 5675 50  0001 C CNN
	6    1025 5575
	1    0    0    -1  
$EndComp
Text Label 600  5575 0    50   ~ 0
~MREQb
Wire Wire Line
	600  5575 875  5575
Wire Wire Line
	1175 5575 1225 5575
$Comp
L my_ics:74LS367_sep U16
U 5 1 5F8AD061
P 1425 5575
F 0 "U16" H 1425 5800 50  0000 C CNN
F 1 "74LS367" H 1425 5709 50  0000 C CNN
F 2 "" H 1525 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 1425 5675 50  0001 C CNN
	5    1425 5575
	1    0    0    -1  
$EndComp
$Comp
L my_ics:74LS367_sep U16
U 4 1 5F8AE95D
P 1825 5575
F 0 "U16" H 1825 5800 50  0000 C CNN
F 1 "74LS367" H 1825 5709 50  0000 C CNN
F 2 "" H 1925 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 1825 5675 50  0001 C CNN
	4    1825 5575
	1    0    0    -1  
$EndComp
Wire Wire Line
	1575 5575 1625 5575
Connection ~ 1225 5575
Wire Wire Line
	1225 5575 1275 5575
Connection ~ 1625 5575
Wire Wire Line
	1625 5575 1675 5575
$Comp
L my_ics:74LS367_sep U16
U 3 1 5F8B5A3C
P 3650 5300
F 0 "U16" H 3650 5525 50  0000 C CNN
F 1 "74LS367" H 3650 5434 50  0000 C CNN
F 2 "" H 3750 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 3650 5400 50  0001 C CNN
	3    3650 5300
	1    0    0    -1  
$EndComp
Connection ~ 2025 5575
Wire Wire Line
	1975 5575 2025 5575
$Comp
L my_ics:74LS367_sep U16
U 2 1 5F8B7E03
P 3250 5300
F 0 "U16" H 3250 5525 50  0000 C CNN
F 1 "74LS367" H 3250 5434 50  0000 C CNN
F 2 "" H 3350 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 3250 5400 50  0001 C CNN
	2    3250 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1225 5100 1225 5575
Wire Wire Line
	1625 5200 1625 5575
Wire Wire Line
	2025 5300 2025 5575
Wire Wire Line
	2625 5400 2725 5400
Wire Wire Line
	2925 5100 2975 5100
Wire Wire Line
	2975 5100 2975 5200
Wire Wire Line
	2975 5300 2925 5300
Wire Wire Line
	2925 5200 2975 5200
Connection ~ 2975 5200
Wire Wire Line
	2975 5200 2975 5300
Connection ~ 2975 5300
Wire Wire Line
	3100 5300 3050 5300
Text Label 3500 5575 2    50   ~ 0
ROW{slash}COL
Wire Wire Line
	3050 5575 3050 5300
Wire Wire Line
	3050 5575 3500 5575
Connection ~ 3050 5300
Wire Wire Line
	2975 5300 3050 5300
$Comp
L Connector_Generic:Conn_02x04_Counter_Clockwise J?
U 1 1 5F8C3BAB
P 2625 5200
F 0 "J?" H 2675 5517 50  0000 C CNN
F 1 "RAM Timing" H 2675 5426 50  0000 C CNN
F 2 "" H 2625 5200 50  0001 C CNN
F 3 "~" H 2625 5200 50  0001 C CNN
	1    2625 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2025 5575 2075 5575
Wire Wire Line
	1225 5100 2425 5100
Wire Wire Line
	1625 5200 2425 5200
Wire Wire Line
	2025 5300 2425 5300
Wire Wire Line
	2425 5400 2400 5400
Wire Wire Line
	2400 5400 2400 5575
Wire Wire Line
	2400 5575 2375 5575
Wire Wire Line
	2925 5400 2975 5400
Wire Wire Line
	2975 5400 2975 5300
Wire Wire Line
	3500 5300 3450 5300
$Comp
L Connector_Generic:Conn_02x02_Counter_Clockwise J?
U 1 1 5F8D9785
P 4050 5200
F 0 "J?" H 4100 5417 50  0000 C CNN
F 1 "RAM Timing" H 4100 5326 50  0000 C CNN
F 2 "" H 4050 5200 50  0001 C CNN
F 3 "~" H 4050 5200 50  0001 C CNN
	1    4050 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 5300 3800 5300
Wire Wire Line
	3850 5200 3825 5200
Wire Wire Line
	3825 5200 3825 5025
Wire Wire Line
	3825 5025 3450 5025
Wire Wire Line
	3450 5025 3450 5300
Connection ~ 3450 5300
Wire Wire Line
	3450 5300 3400 5300
Wire Wire Line
	4375 5300 4350 5300
Wire Wire Line
	4350 5200 4375 5200
Wire Wire Line
	4375 5200 4375 5300
Connection ~ 4375 5300
Wire Wire Line
	4050 5300 4150 5300
Wire Notes Line
	575  5675 4450 5675
Wire Notes Line
	4450 5675 4450 4925
Wire Notes Line
	4450 4925 2850 4925
Wire Notes Line
	2850 4925 2850 4800
Wire Notes Line
	2850 4800 575  4800
Wire Notes Line
	575  4800 575  5675
Text Notes 750  4975 0    50   ~ 0
should be right
Wire Wire Line
	2850 7475 2975 7475
Wire Wire Line
	2975 7475 2975 7300
Wire Wire Line
	2975 7300 2175 7075
Wire Wire Line
	2175 7075 2175 7000
Wire Wire Line
	2250 7000 2175 7000
Wire Wire Line
	2250 7375 2175 7375
Wire Wire Line
	2175 7375 2175 7300
Wire Wire Line
	2175 7300 2975 7075
Wire Wire Line
	2975 7075 2975 6900
Wire Wire Line
	2975 6900 2850 6900
Text Label 550  6375 0    50   ~ 0
A14
Wire Wire Line
	750  6275 550  6275
Text Label 550  6275 0    50   ~ 0
A15
Wire Wire Line
	2025 6500 2025 6275
Wire Wire Line
	1600 6500 2025 6500
Wire Wire Line
	1600 6700 1600 6500
Wire Wire Line
	1650 6700 1600 6700
Text Label 575  6800 0    50   ~ 0
~RDb
Connection ~ 750  6275
Wire Wire Line
	2000 6275 2025 6275
Wire Wire Line
	775  6075 750  6075
Wire Wire Line
	750  6275 775  6275
Wire Wire Line
	750  6075 750  6275
$Comp
L 74xx:74LS00 U9
U 3 1 5EFBA41D
P 1700 6275
F 0 "U9" H 1675 6275 50  0000 C CNN
F 1 "74LS00" H 1900 6125 50  0000 C CNN
F 2 "" H 1700 6275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1700 6275 50  0001 C CNN
	3    1700 6275
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS00 U9
U 4 1 5EFBB114
P 1075 6175
F 0 "U9" H 1050 6175 50  0000 C CNN
F 1 "74LS00" H 1325 6325 50  0000 C CNN
F 2 "" H 1075 6175 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1075 6175 50  0001 C CNN
	4    1075 6175
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 6175 1400 6175
Wire Wire Line
	1400 6375 550  6375
Text Label 575  7000 0    50   ~ 0
~MREQb
$Comp
L 74xx:74LS32 U10
U 3 1 5F06A8D0
P 1950 6800
F 0 "U10" H 1975 6800 50  0000 C CNN
F 1 "74LS32" H 1925 7000 50  0000 C CNN
F 2 "" H 1950 6800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 1950 6800 50  0001 C CNN
	3    1950 6800
	1    0    0    -1  
$EndComp
Connection ~ 2975 6900
Wire Wire Line
	3600 6800 3625 6800
Connection ~ 3625 6800
Wire Wire Line
	3625 6800 3625 7075
$Comp
L 74xx:74LS00 U8
U 4 1 5EF8E072
P 4000 6700
AR Path="/5EFDD352/5EF8E072" Ref="U8"  Part="4" 
AR Path="/5EFDE485/5EF8E072" Ref="U8"  Part="4" 
F 0 "U8" H 3975 6700 50  0000 C CNN
F 1 "74LS00" H 4225 6850 50  0000 C CNN
F 2 "" H 4000 6700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 4000 6700 50  0001 C CNN
	4    4000 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3625 6800 3700 6800
Wire Wire Line
	4375 5300 4525 5300
Wire Wire Line
	5275 1875 5275 5400
Wire Wire Line
	5275 5400 5125 5400
Wire Wire Line
	4475 5500 4525 5500
Wire Wire Line
	2775 6700 3000 6700
Wire Wire Line
	2975 6900 3000 6900
Wire Wire Line
	4300 6700 4475 6700
Wire Wire Line
	4475 5500 4475 6700
Wire Wire Line
	575  6800 875  6800
Wire Wire Line
	575  7000 875  7000
Wire Wire Line
	1475 6900 1650 6900
Connection ~ 5275 5400
Wire Wire Line
	5275 1875 6900 1875
$EndSCHEMATC
