EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Wire Wire Line
	1600 1275 1275 1275
Wire Wire Line
	1600 2075 1275 2075
Wire Wire Line
	1600 1775 1275 1775
Wire Wire Line
	1600 1475 1275 1475
Wire Wire Line
	1600 1675 1275 1675
Wire Wire Line
	1600 1975 1275 1975
Wire Wire Line
	1600 2275 1275 2275
Wire Wire Line
	1600 1175 1275 1175
Wire Wire Line
	1600 1075 1275 1075
Wire Wire Line
	1600 1375 1275 1375
Text Label 1600 1775 2    50   ~ 0
~RD
Text Label 1600 1675 2    50   ~ 0
~HALT
Text Label 1600 1975 2    50   ~ 0
~MREQ
Text Label 1600 2075 2    50   ~ 0
~IORQ
Text Label 1600 1375 2    50   ~ 0
~M1
Text Label 1600 2175 2    50   ~ 0
~BUSRQ
Text Label 1600 1175 2    50   ~ 0
~NMI
Text Label 1600 1575 2    50   ~ 0
~WAIT
Text Label 1600 1075 2    50   ~ 0
~CLK
Text Label 1600 2275 2    50   ~ 0
~BUSACK
Text Label 1600 1475 2    50   ~ 0
~RFSH
Text Label 1600 1275 2    50   ~ 0
~INT
Wire Wire Line
	1600 975  1275 975 
Wire Wire Line
	1600 1875 1275 1875
Text Label 1600 1875 2    50   ~ 0
~WR
Text Label 1600 975  2    50   ~ 0
~RESET
Wire Wire Line
	1600 1575 1275 1575
Wire Wire Line
	1600 2175 1275 2175
Wire Wire Line
	2025 2200 2300 2200
Wire Wire Line
	2025 1100 2300 1100
Wire Wire Line
	2600 1700 2875 1700
Wire Wire Line
	2600 2000 2875 2000
Wire Wire Line
	2025 2100 2300 2100
Wire Wire Line
	2025 800  2300 800 
Wire Wire Line
	2025 1500 2300 1500
Wire Wire Line
	2025 1600 2300 1600
Wire Wire Line
	2025 900  2300 900 
Wire Wire Line
	2025 1200 2300 1200
Wire Wire Line
	2025 1900 2300 1900
Wire Wire Line
	2025 1400 2300 1400
Wire Wire Line
	2600 1800 2875 1800
Wire Wire Line
	2025 1700 2300 1700
Text Label 2300 1200 2    50   ~ 0
A4
Wire Wire Line
	2600 2200 2875 2200
Text Label 2300 1300 2    50   ~ 0
A5
Wire Wire Line
	2025 2000 2300 2000
Wire Wire Line
	2600 1600 2875 1600
Wire Wire Line
	2600 1900 2875 1900
Wire Wire Line
	2025 1000 2300 1000
Wire Wire Line
	2025 1800 2300 1800
Text Label 2875 1800 2    50   ~ 0
D2
Wire Wire Line
	2600 2300 2875 2300
Text Label 2300 1900 2    50   ~ 0
A11
Text Label 2875 1700 2    50   ~ 0
D1
Text Label 2875 1600 2    50   ~ 0
D0
Text Label 2300 1600 2    50   ~ 0
A8
Text Label 2300 1400 2    50   ~ 0
A6
Text Label 2300 1500 2    50   ~ 0
A7
Wire Wire Line
	2025 2300 2300 2300
Text Label 2875 1900 2    50   ~ 0
D3
Text Label 2300 1800 2    50   ~ 0
A10
Text Label 2300 2300 2    50   ~ 0
A15
Text Label 2875 2000 2    50   ~ 0
D4
Text Label 2875 2300 2    50   ~ 0
D7
Text Label 2875 2100 2    50   ~ 0
D5
Text Label 2875 2200 2    50   ~ 0
D6
Text Label 2300 2100 2    50   ~ 0
A13
Text Label 2300 2200 2    50   ~ 0
A14
Text Label 2300 1000 2    50   ~ 0
A2
Text Label 2300 800  2    50   ~ 0
A0
Text Label 2300 1100 2    50   ~ 0
A3
Text Label 2300 2000 2    50   ~ 0
A12
Text Label 2300 900  2    50   ~ 0
A1
Wire Wire Line
	2025 1300 2300 1300
Text Label 2300 1700 2    50   ~ 0
A9
Wire Wire Line
	2600 2100 2875 2100
Text HLabel 2600 1600 0    50   Input ~ 0
D0
Text HLabel 2600 1700 0    50   Input ~ 0
D1
Text HLabel 2600 1800 0    50   Input ~ 0
D2
Text HLabel 2600 1900 0    50   Input ~ 0
D3
Text HLabel 2600 2000 0    50   Input ~ 0
D4
Text HLabel 2600 2100 0    50   Input ~ 0
D5
Text HLabel 2600 2200 0    50   Input ~ 0
D6
Text HLabel 2600 2300 0    50   Input ~ 0
D7
Text HLabel 2025 800  0    50   Input ~ 0
A0
Text HLabel 2025 900  0    50   Input ~ 0
A1
Text HLabel 2025 1000 0    50   Input ~ 0
A2
Text HLabel 2025 1100 0    50   Input ~ 0
A3
Text HLabel 2025 1200 0    50   Input ~ 0
A4
Text HLabel 2025 1300 0    50   Input ~ 0
A5
Text HLabel 2025 1400 0    50   Input ~ 0
A6
Text HLabel 2025 1500 0    50   Input ~ 0
A7
Text HLabel 2025 1600 0    50   Input ~ 0
A8
Text HLabel 2025 1700 0    50   Input ~ 0
A9
Text HLabel 2025 1800 0    50   Input ~ 0
A10
Text HLabel 2025 1900 0    50   Input ~ 0
A11
Text HLabel 2025 2000 0    50   Input ~ 0
A12
Text HLabel 2025 2100 0    50   Input ~ 0
A13
Text HLabel 2025 2200 0    50   Input ~ 0
A14
Text HLabel 2025 2300 0    50   Input ~ 0
A15
Text HLabel 1275 975  0    50   Input ~ 0
~RESET
Text HLabel 1275 1075 0    50   Input ~ 0
~CLK
Text HLabel 1275 1175 0    50   Input ~ 0
~NMI
Text HLabel 1275 1275 0    50   Input ~ 0
~INT
Text HLabel 1275 1375 0    50   Input ~ 0
~M1
Text HLabel 1275 1475 0    50   Input ~ 0
~RFSH
Text HLabel 1275 1575 0    50   Input ~ 0
~WAIT
Text HLabel 1275 1675 0    50   Input ~ 0
~HALT
Text HLabel 1275 1775 0    50   Input ~ 0
~RD
Text HLabel 1275 1875 0    50   Input ~ 0
~WR
Text HLabel 1275 1975 0    50   Input ~ 0
~MREQ
Text HLabel 1275 2075 0    50   Input ~ 0
~IORQ
Text HLabel 1275 2175 0    50   Input ~ 0
~BUSRQ
Text HLabel 1275 2275 0    50   Input ~ 0
~BUSACK
$Comp
L 74xx:74LS245 U6
U 1 1 5EF44186
P 6150 6800
AR Path="/5EFDD352/5EF44186" Ref="U6"  Part="1" 
AR Path="/5EFDE485/5EF44186" Ref="U?"  Part="1" 
F 0 "U6" H 5800 7525 50  0000 C CNN
F 1 "74LS245" H 5900 7450 50  0000 C CNN
F 2 "" H 6150 6800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 6150 6800 50  0001 C CNN
	1    6150 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2450 8750 2450
Wire Wire Line
	8400 2550 8750 2550
Wire Wire Line
	8400 2650 8750 2650
Wire Wire Line
	8400 2750 8750 2750
Wire Wire Line
	8400 2850 8750 2850
Wire Wire Line
	8400 2950 8750 2950
Wire Wire Line
	8400 3050 8750 3050
Wire Wire Line
	8400 3150 8750 3150
Wire Wire Line
	8400 3250 8750 3250
Wire Wire Line
	8400 3350 8750 3350
Wire Wire Line
	8400 3450 8750 3450
Wire Wire Line
	8400 4150 8750 4150
Wire Wire Line
	8400 4350 8750 4350
Wire Wire Line
	8400 5050 8750 5050
Wire Wire Line
	8400 5150 8750 5150
Wire Wire Line
	8400 5250 8750 5250
Wire Wire Line
	8400 5350 8750 5350
Wire Wire Line
	8400 5450 8750 5450
Wire Wire Line
	8400 5550 8450 5550
Wire Wire Line
	9700 4375 10050 4375
Wire Wire Line
	9700 4775 10050 4775
Wire Wire Line
	9700 3075 10050 3075
Wire Wire Line
	9700 4875 10050 4875
Wire Wire Line
	9700 5575 9775 5575
Wire Wire Line
	9700 2775 10050 2775
Wire Wire Line
	9700 2875 10050 2875
Wire Wire Line
	9700 5275 10050 5275
Wire Wire Line
	9700 5375 10050 5375
Wire Wire Line
	9700 5075 10050 5075
Wire Wire Line
	9700 4975 10050 4975
Wire Wire Line
	9700 5475 10050 5475
Wire Wire Line
	9700 2575 10050 2575
Wire Wire Line
	9700 5175 10050 5175
Wire Wire Line
	9700 2975 10050 2975
Wire Wire Line
	9700 3275 10050 3275
Wire Wire Line
	9700 3775 10050 3775
Wire Wire Line
	9700 2675 10050 2675
Wire Wire Line
	9700 4175 10050 4175
Wire Wire Line
	9700 4575 10050 4575
Wire Wire Line
	9700 3175 10050 3175
Wire Wire Line
	9700 4475 10050 4475
Wire Wire Line
	9700 3375 10050 3375
Wire Wire Line
	9700 4075 10050 4075
$Comp
L power:+5V #PWR?
U 1 1 5EF7E44E
P 6150 6000
AR Path="/5EFDD352/5EF7E44E" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF7E44E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6150 5850 50  0001 C CNN
F 1 "+5V" H 6150 6150 50  0000 C CNN
F 2 "" H 6150 6000 50  0001 C CNN
F 3 "" H 6150 6000 50  0001 C CNN
	1    6150 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF82005
P 6150 7600
AR Path="/5EFDD352/5EF82005" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF82005" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6150 7350 50  0001 C CNN
F 1 "GND" H 6150 7450 50  0000 C CNN
F 2 "" H 6150 7600 50  0001 C CNN
F 3 "" H 6150 7600 50  0001 C CNN
	1    6150 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 6300 6800 6300
Wire Wire Line
	6650 6400 6800 6400
Wire Wire Line
	6650 6500 6800 6500
Wire Wire Line
	6650 6600 6800 6600
Wire Wire Line
	6650 6700 6800 6700
Wire Wire Line
	6650 6800 6800 6800
Wire Wire Line
	6650 6900 6800 6900
Wire Wire Line
	6650 7000 6800 7000
Wire Wire Line
	5475 6400 5650 6400
Wire Wire Line
	5475 6700 5650 6700
Wire Wire Line
	5475 6900 5650 6900
Wire Wire Line
	5475 6300 5650 6300
Wire Wire Line
	5475 6500 5650 6500
Wire Wire Line
	5475 6800 5650 6800
Wire Wire Line
	5475 6600 5650 6600
Wire Wire Line
	5475 7000 5650 7000
Wire Wire Line
	5575 7200 5650 7200
Text Label 6800 6400 2    50   ~ 0
D1
Text Label 6800 6300 2    50   ~ 0
D0
Text Label 6800 6500 2    50   ~ 0
D2
Text Label 6800 6700 2    50   ~ 0
D4
Text Label 6800 6600 2    50   ~ 0
D3
Text Label 6800 6900 2    50   ~ 0
D6
Text Label 6800 7000 2    50   ~ 0
D7
Text Label 6800 6800 2    50   ~ 0
D5
Text Label 5475 6300 0    50   ~ 0
DB0
Text Label 5475 6400 0    50   ~ 0
DB1
Text Label 5475 6500 0    50   ~ 0
DB2
Text Label 5475 6600 0    50   ~ 0
DB3
Text Label 5475 6700 0    50   ~ 0
DB4
Text Label 5475 6800 0    50   ~ 0
DB5
Text Label 5475 6900 0    50   ~ 0
DB6
Text Label 5475 7000 0    50   ~ 0
DB7
$Comp
L 74xx:74LS00 U8
U 2 1 5EF8BA16
P 5275 7200
AR Path="/5EFDD352/5EF8BA16" Ref="U8"  Part="2" 
AR Path="/5EFDE485/5EF8BA16" Ref="U?"  Part="2" 
F 0 "U8" H 5275 7525 50  0000 C CNN
F 1 "74LS00" H 5275 7434 50  0000 C CNN
F 2 "" H 5275 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5275 7200 50  0001 C CNN
	2    5275 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF9D3F2
P 5600 7350
AR Path="/5EFDD352/5EF9D3F2" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EF9D3F2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 7100 50  0001 C CNN
F 1 "GND" H 5605 7177 50  0000 C CNN
F 2 "" H 5600 7350 50  0001 C CNN
F 3 "" H 5600 7350 50  0001 C CNN
	1    5600 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 7350 5600 7300
Wire Wire Line
	5600 7300 5650 7300
$Comp
L my_conn:CONN_DIN41612_32x3 P?
U 1 1 5EFA257C
P 8200 4000
AR Path="/5EFDD352/5EFA257C" Ref="P?"  Part="1" 
AR Path="/5EFDE485/5EFA257C" Ref="P?"  Part="1" 
F 0 "P?" H 8117 5765 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 8117 5674 50  0000 C CNN
F 2 "" H 8200 4000 50  0000 C CNN
F 3 "" H 8200 4000 50  0000 C CNN
	1    8200 4000
	-1   0    0    -1  
$EndComp
$Comp
L my_conn:CONN_DIN41612_32x3 P?
U 3 1 5EFA60F5
P 9500 4025
AR Path="/5EFDD352/5EFA60F5" Ref="P?"  Part="3" 
AR Path="/5EFDE485/5EFA60F5" Ref="P?"  Part="3" 
F 0 "P?" H 9417 5790 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 9417 5699 50  0000 C CNN
F 2 "" H 9500 4025 50  0000 C CNN
F 3 "" H 9500 4025 50  0000 C CNN
	3    9500 4025
	-1   0    0    -1  
$EndComp
Text Label 10050 2575 2    50   ~ 0
DB0
Text Label 10050 2675 2    50   ~ 0
DB7
Text Label 10050 2775 2    50   ~ 0
DB2
Text Label 8750 2650 2    50   ~ 0
DB6
Text Label 10050 3775 2    50   ~ 0
DB1
Text Label 8750 2750 2    50   ~ 0
DB3
Text Label 8750 2850 2    50   ~ 0
DB4
Text Label 8750 2550 2    50   ~ 0
DB5
Wire Wire Line
	9700 2475 10050 2475
$Comp
L power:+5V #PWR?
U 1 1 5EFBB209
P 10050 2275
AR Path="/5EFDD352/5EFBB209" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBB209" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 2125 50  0001 C CNN
F 1 "+5V" H 10065 2448 50  0000 C CNN
F 2 "" H 10050 2275 50  0001 C CNN
F 3 "" H 10050 2275 50  0001 C CNN
	1    10050 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2275 10050 2475
$Comp
L power:+5V #PWR?
U 1 1 5EFBC250
P 8750 2250
AR Path="/5EFDD352/5EFBC250" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBC250" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8750 2100 50  0001 C CNN
F 1 "+5V" H 8765 2423 50  0000 C CNN
F 2 "" H 8750 2250 50  0001 C CNN
F 3 "" H 8750 2250 50  0001 C CNN
	1    8750 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2250 8750 2450
$Comp
L power:GND #PWR?
U 1 1 5EFBCA20
P 9775 5625
AR Path="/5EFDD352/5EFBCA20" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBCA20" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9775 5375 50  0001 C CNN
F 1 "GND" H 9780 5452 50  0000 C CNN
F 2 "" H 9775 5625 50  0001 C CNN
F 3 "" H 9775 5625 50  0001 C CNN
	1    9775 5625
	1    0    0    -1  
$EndComp
Wire Wire Line
	9775 5625 9775 5575
Wire Wire Line
	8450 5600 8450 5550
$Comp
L power:GND #PWR?
U 1 1 5EFBD4EE
P 8450 5600
AR Path="/5EFDD352/5EFBD4EE" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBD4EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8450 5350 50  0001 C CNN
F 1 "GND" H 8455 5427 50  0000 C CNN
F 2 "" H 8450 5600 50  0001 C CNN
F 3 "" H 8450 5600 50  0001 C CNN
	1    8450 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4675 6625 4675
$Comp
L power:+5V #PWR?
U 1 1 5EFBDE07
P 6000 4275
AR Path="/5EFDD352/5EFBDE07" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBDE07" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6000 4125 50  0001 C CNN
F 1 "+5V" H 6000 4425 50  0000 C CNN
F 2 "" H 6000 4275 50  0001 C CNN
F 3 "" H 6000 4275 50  0001 C CNN
	1    6000 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5325 5275 5500 5275
Wire Wire Line
	6500 4975 6625 4975
Wire Wire Line
	5450 5625 5450 5575
Wire Wire Line
	6500 5175 6625 5175
Wire Wire Line
	6500 4575 6625 4575
Wire Wire Line
	5325 4875 5500 4875
Wire Wire Line
	5325 4675 5500 4675
Wire Wire Line
	5450 5575 5500 5575
$Comp
L power:GND #PWR?
U 1 1 5EFBDE1F
P 5450 5625
AR Path="/5EFDD352/5EFBDE1F" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBDE1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5450 5375 50  0001 C CNN
F 1 "GND" H 5455 5452 50  0000 C CNN
F 2 "" H 5450 5625 50  0001 C CNN
F 3 "" H 5450 5625 50  0001 C CNN
	1    5450 5625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFBDE2E
P 6000 5875
AR Path="/5EFDD352/5EFBDE2E" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBDE2E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6000 5625 50  0001 C CNN
F 1 "GND" H 6000 5725 50  0000 C CNN
F 2 "" H 6000 5875 50  0001 C CNN
F 3 "" H 6000 5875 50  0001 C CNN
	1    6000 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	5325 5075 5500 5075
Wire Wire Line
	5325 5175 5500 5175
Wire Wire Line
	5325 4975 5500 4975
Wire Wire Line
	6500 4775 6625 4775
Wire Wire Line
	6500 5075 6625 5075
Wire Wire Line
	6500 4875 6625 4875
Wire Wire Line
	5325 4575 5500 4575
Wire Wire Line
	5325 4775 5500 4775
$Comp
L 74xx:74LS245 U5
U 1 1 5EFBDE5D
P 6000 5075
AR Path="/5EFDD352/5EFBDE5D" Ref="U5"  Part="1" 
AR Path="/5EFDE485/5EFBDE5D" Ref="U?"  Part="1" 
F 0 "U5" H 5650 5800 50  0000 C CNN
F 1 "74LS245" H 5750 5725 50  0000 C CNN
F 2 "" H 6000 5075 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 6000 5075 50  0001 C CNN
	1    6000 5075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5275 6625 5275
Text Label 6625 4575 2    50   ~ 0
A0
Text Label 6625 5175 2    50   ~ 0
A6
Text Label 6625 5075 2    50   ~ 0
A5
Text Label 6625 4775 2    50   ~ 0
A2
Text Label 6625 4975 2    50   ~ 0
A4
Text Label 6625 5275 2    50   ~ 0
A7
Text Label 6625 4675 2    50   ~ 0
A1
Text Label 6625 4875 2    50   ~ 0
A3
Text Label 6625 4575 2    50   ~ 0
A0
Text Label 5325 4575 0    50   ~ 0
AB0
Text Label 5325 4675 0    50   ~ 0
AB1
Text Label 5325 4775 0    50   ~ 0
AB2
Text Label 5325 4875 0    50   ~ 0
AB3
Text Label 5325 4975 0    50   ~ 0
AB4
Text Label 5325 5075 0    50   ~ 0
AB5
Text Label 5325 5175 0    50   ~ 0
AB6
Text Label 5325 5275 0    50   ~ 0
AB7
Wire Wire Line
	6325 3150 6500 3150
Wire Wire Line
	5125 3350 5325 3350
$Comp
L 74xx:74LS245 U4
U 1 1 5EFC813E
P 5825 3350
AR Path="/5EFDD352/5EFC813E" Ref="U4"  Part="1" 
AR Path="/5EFDE485/5EFC813E" Ref="U?"  Part="1" 
F 0 "U4" H 5475 4075 50  0000 C CNN
F 1 "74LS245" H 5575 4000 50  0000 C CNN
F 2 "" H 5825 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 5825 3350 50  0001 C CNN
	1    5825 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6325 3050 6500 3050
$Comp
L power:GND #PWR?
U 1 1 5EFC814C
P 5275 3900
AR Path="/5EFDD352/5EFC814C" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFC814C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5275 3650 50  0001 C CNN
F 1 "GND" H 5280 3727 50  0000 C CNN
F 2 "" H 5275 3900 50  0001 C CNN
F 3 "" H 5275 3900 50  0001 C CNN
	1    5275 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFC8159
P 5825 4150
AR Path="/5EFDD352/5EFC8159" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFC8159" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5825 3900 50  0001 C CNN
F 1 "GND" H 5825 4000 50  0000 C CNN
F 2 "" H 5825 4150 50  0001 C CNN
F 3 "" H 5825 4150 50  0001 C CNN
	1    5825 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 3250 5325 3250
Wire Wire Line
	6325 2950 6500 2950
Wire Wire Line
	6325 3450 6500 3450
Wire Wire Line
	5275 3850 5325 3850
Wire Wire Line
	5275 3900 5275 3850
Wire Wire Line
	5125 3050 5325 3050
Wire Wire Line
	6325 2850 6500 2850
Wire Wire Line
	6325 3550 6500 3550
Wire Wire Line
	5125 3550 5325 3550
Wire Wire Line
	6325 3250 6500 3250
Wire Wire Line
	6325 3350 6500 3350
Wire Wire Line
	5125 3150 5325 3150
Wire Wire Line
	5125 3450 5325 3450
Wire Wire Line
	5125 2950 5325 2950
Wire Wire Line
	5125 2850 5325 2850
Text Label 6500 3550 2    50   ~ 0
A15
Text Label 6500 2950 2    50   ~ 0
A9
Text Label 6500 3150 2    50   ~ 0
A11
Text Label 6500 3050 2    50   ~ 0
A10
Text Label 6500 3350 2    50   ~ 0
A13
Text Label 6500 3250 2    50   ~ 0
A12
Text Label 6500 2850 2    50   ~ 0
A8
Text Label 6500 3450 2    50   ~ 0
A14
Text Label 5125 2850 0    50   ~ 0
AB8
Text Label 5125 2950 0    50   ~ 0
AB9
Text Label 5125 3050 0    50   ~ 0
AB10
Text Label 5125 3150 0    50   ~ 0
AB11
Text Label 5125 3250 0    50   ~ 0
AB12
Text Label 5125 3350 0    50   ~ 0
AB13
Text Label 5125 3450 0    50   ~ 0
AB14
Text Label 5125 3550 0    50   ~ 0
AB15
Text Label 8750 3150 2    50   ~ 0
AB5
Text Label 10050 2875 2    50   ~ 0
AB0
Text Label 8750 2950 2    50   ~ 0
AB2
Text Label 8750 3050 2    50   ~ 0
AB4
Text Label 10050 3275 2    50   ~ 0
AB7
Text Label 10050 3075 2    50   ~ 0
AB1
Text Label 8750 3250 2    50   ~ 0
AB6
Text Label 10050 2975 2    50   ~ 0
AB3
Text Label 10050 5175 2    50   ~ 0
AB12
Text Label 8750 4150 2    50   ~ 0
AB14
Text Label 10050 4075 2    50   ~ 0
AB11
Text Label 10050 4175 2    50   ~ 0
AB10
Text Label 10050 3175 2    50   ~ 0
AB8
Text Label 10050 5075 2    50   ~ 0
AB15
Text Label 8750 5250 2    50   ~ 0
AB13
Text Label 8750 5350 2    50   ~ 0
AB9
Text Label 8750 3350 2    50   ~ 0
~WAIT
Text Label 8750 3450 2    50   ~ 0
~BUSRQ
$Comp
L power:+5V #PWR?
U 1 1 5EFB56B0
P 5825 2550
AR Path="/5EFDD352/5EFB56B0" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFB56B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5825 2400 50  0001 C CNN
F 1 "+5V" H 5825 2700 50  0000 C CNN
F 2 "" H 5825 2550 50  0001 C CNN
F 3 "" H 5825 2550 50  0001 C CNN
	1    5825 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1425 6400 1425
Wire Wire Line
	4950 1625 5150 1625
$Comp
L power:+5V #PWR?
U 1 1 5EFB56BF
P 5650 825
AR Path="/5EFDD352/5EFB56BF" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFB56BF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 675 50  0001 C CNN
F 1 "+5V" H 5665 998 50  0000 C CNN
F 2 "" H 5650 825 50  0001 C CNN
F 3 "" H 5650 825 50  0001 C CNN
	1    5650 825 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U3
U 1 1 5EFB56DF
P 5650 1625
AR Path="/5EFDD352/5EFB56DF" Ref="U3"  Part="1" 
AR Path="/5EFDE485/5EFB56DF" Ref="U?"  Part="1" 
F 0 "U3" H 5300 2350 50  0000 C CNN
F 1 "74LS245" H 5400 2275 50  0000 C CNN
F 2 "" H 5650 1625 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 5650 1625 50  0001 C CNN
	1    5650 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1325 6400 1325
$Comp
L power:GND #PWR?
U 1 1 5EFB56FA
P 5650 2425
AR Path="/5EFDD352/5EFB56FA" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFB56FA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 2175 50  0001 C CNN
F 1 "GND" H 5650 2275 50  0000 C CNN
F 2 "" H 5650 2425 50  0001 C CNN
F 3 "" H 5650 2425 50  0001 C CNN
	1    5650 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1525 5150 1525
Wire Wire Line
	4950 1325 5150 1325
Wire Wire Line
	4950 1825 5150 1825
Wire Wire Line
	6150 1525 6400 1525
Wire Wire Line
	4950 1425 5150 1425
Wire Wire Line
	4950 1725 5150 1725
Text Label 10050 4575 2    50   ~ 0
~WRb
Text Label 6400 1525 2    50   ~ 0
~WRb
Text Label 4950 1525 0    50   ~ 0
~WR
Text Label 4950 1625 0    50   ~ 0
~RD
Text Label 6400 1625 2    50   ~ 0
~RDb
Text Label 10050 4775 2    50   ~ 0
~RDb
$Comp
L power:+5V #PWR?
U 1 1 5EFBC0D4
P 4800 1925
AR Path="/5EFDD352/5EFBC0D4" Ref="#PWR?"  Part="1" 
AR Path="/5EFDE485/5EFBC0D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 1775 50  0001 C CNN
F 1 "+5V" H 4800 2075 50  0000 C CNN
F 2 "" H 4800 1925 50  0001 C CNN
F 3 "" H 4800 1925 50  0001 C CNN
	1    4800 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1925 4800 2025
Wire Wire Line
	4800 2025 5150 2025
Text Label 4950 1325 0    50   ~ 0
~M1
Text Label 6400 1325 2    50   ~ 0
~M1b
Text Label 8750 4350 2    50   ~ 0
~M1b
Text Label 4950 1425 0    50   ~ 0
~RFSH
Text Label 6400 1425 2    50   ~ 0
~RFSHb
Text Label 8750 5150 2    50   ~ 0
~RFSHb
Text Label 4950 1725 0    50   ~ 0
~IORQ
Text Label 4950 1825 0    50   ~ 0
~MREQ
Text Label 6400 1725 2    50   ~ 0
~IORQb
Text Label 6400 1825 2    50   ~ 0
~MREQb
Wire Wire Line
	6150 1725 6400 1725
Wire Wire Line
	6150 1825 6400 1825
Wire Wire Line
	6150 1625 6400 1625
Text Label 10050 5375 2    50   ~ 0
~MREQb
Text Label 8750 5050 2    50   ~ 0
~IORQb
$Comp
L 74xx:74LS32 U7
U 1 1 5EFC2F4C
P 3550 7000
F 0 "U7" H 3550 7325 50  0000 C CNN
F 1 "74LS32" H 3550 7234 50  0000 C CNN
F 2 "" H 3550 7000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3550 7000 50  0001 C CNN
	1    3550 7000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U7
U 2 1 5EFC096D
P 4525 7100
F 0 "U7" H 4525 7425 50  0000 C CNN
F 1 "74LS32" H 4525 7334 50  0000 C CNN
F 2 "" H 4525 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4525 7100 50  0001 C CNN
	2    4525 7100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U11
U 3 1 5EFAC4C9
P 6425 850
F 0 "U11" H 6425 1167 50  0000 C CNN
F 1 "74LS04" H 6425 1076 50  0000 C CNN
F 2 "" H 6425 850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6425 850 50  0001 C CNN
	3    6425 850 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U11
U 2 1 5EFABE3F
P 4400 2125
F 0 "U11" H 4400 2442 50  0000 C CNN
F 1 "74LS04" H 4400 2351 50  0000 C CNN
F 2 "" H 4400 2125 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 4400 2125 50  0001 C CNN
	2    4400 2125
	1    0    0    -1  
$EndComp
Text Label 3750 2125 0    50   ~ 0
~BUSACK
Wire Wire Line
	4100 2125 3750 2125
Text Label 7075 850  2    50   ~ 0
~BUSACKb
Wire Wire Line
	7075 850  6725 850 
Text Label 5775 850  0    50   ~ 0
BUSACKb
Text Label 4950 3750 0    50   ~ 0
BUSACKb
Text Label 4725 2125 0    50   ~ 0
BUSACKb
Text Label 3875 7200 0    50   ~ 0
BUSACKb
Wire Wire Line
	3875 7200 4225 7200
Text Label 5150 5475 0    50   ~ 0
BUSACKb
Text Label 8750 5450 2    50   ~ 0
~BUSACKb
Wire Wire Line
	3850 7000 4225 7000
Wire Wire Line
	4975 7100 4825 7100
Wire Wire Line
	8875 1425 9100 1425
$Comp
L 74xx:74LS04 U11
U 5 1 5EFACF62
P 9400 1425
F 0 "U11" H 9400 1742 50  0000 C CNN
F 1 "74LS04" H 9400 1651 50  0000 C CNN
F 2 "" H 9400 1425 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9400 1425 50  0001 C CNN
	5    9400 1425
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U11
U 6 1 5EFAD77E
P 9400 875
F 0 "U11" H 9400 1192 50  0000 C CNN
F 1 "74LS04" H 9400 1101 50  0000 C CNN
F 2 "" H 9400 875 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9400 875 50  0001 C CNN
	6    9400 875 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1425 10025 1425
Text Label 10025 1425 2    50   ~ 0
~RAM_WE
Text Label 3100 800  2    50   ~ 0
~RAM_WE
Wire Wire Line
	3100 800  2775 800 
Text HLabel 2775 800  0    50   Input ~ 0
~RAM_WE
Wire Wire Line
	3250 6900 3000 6900
Wire Wire Line
	9100 875  8750 875 
Text Label 8750 875  0    50   ~ 0
~WRb
Text Label 9850 875  2    50   ~ 0
WRb
Wire Wire Line
	9850 875  9700 875 
Text Label 8875 1425 0    50   ~ 0
WRb
Text Label 3000 6900 0    50   ~ 0
WRb
Text Label 2975 7100 0    50   ~ 0
~IORQb
Wire Wire Line
	2975 7100 3250 7100
Text Label 3125 925  2    50   ~ 0
~MREQb
Wire Wire Line
	2725 925  3125 925 
Text HLabel 2725 925  0    50   Input ~ 0
~MREQb
Text HLabel 2775 1050 0    50   Input ~ 0
~PER_CLK
Text Label 3175 1050 2    50   ~ 0
~PER_CLK
Wire Wire Line
	3175 1050 2775 1050
Text Label 10050 5275 2    50   ~ 0
~PER_CLK
NoConn ~ 6150 1125
NoConn ~ 6150 1225
NoConn ~ 5150 1125
NoConn ~ 5150 1225
Text Label 10050 5475 2    50   ~ 0
~RESET
Text Label 10050 4975 2    50   ~ 0
~RESET
NoConn ~ 8400 4850
NoConn ~ 8400 4950
NoConn ~ 8400 4650
NoConn ~ 8400 4750
NoConn ~ 8400 4250
NoConn ~ 8400 4450
NoConn ~ 8400 4550
NoConn ~ 8400 3550
NoConn ~ 8400 3650
NoConn ~ 8400 3750
NoConn ~ 8400 3850
NoConn ~ 8400 3950
NoConn ~ 8400 4050
Wire Wire Line
	10150 3975 10150 3475
Wire Wire Line
	9700 3975 10150 3975
Wire Wire Line
	9700 3475 10150 3475
Text Notes 10050 3375 0    50   ~ 0
 not conn.
NoConn ~ 9700 3575
NoConn ~ 9700 3675
NoConn ~ 9700 3875
NoConn ~ 9700 4275
Text Label 10050 4375 2    50   ~ 0
~NMI
Text Label 10050 4475 2    50   ~ 0
~INT
NoConn ~ 9700 4675
Text Label 10050 4875 2    50   ~ 0
~HALT
Wire Wire Line
	3875 7675 4225 7675
Text Label 3875 7675 0    50   ~ 0
~BUSACKb
Wire Wire Line
	3200 7575 3250 7575
Connection ~ 3200 7575
Wire Wire Line
	3200 7375 3250 7375
Wire Wire Line
	3200 7575 3200 7375
Wire Wire Line
	3000 7575 3200 7575
Text Label 3000 7575 0    50   ~ 0
~RDb
Wire Wire Line
	3850 7475 4225 7475
Wire Wire Line
	4875 7575 4825 7575
Wire Wire Line
	4875 7300 4875 7575
Wire Wire Line
	4975 7300 4875 7300
$Comp
L 74xx:74LS32 U7
U 3 1 5EF69EBA
P 4525 7575
F 0 "U7" H 4525 7850 50  0000 C CNN
F 1 "74LS32" H 4525 7775 50  0000 C CNN
F 2 "" H 4525 7575 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4525 7575 50  0001 C CNN
	3    4525 7575
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U8
U 1 1 5EF8A7C3
P 3550 7475
AR Path="/5EFDD352/5EF8A7C3" Ref="U8"  Part="1" 
AR Path="/5EFDE485/5EF8A7C3" Ref="U?"  Part="1" 
F 0 "U8" H 3550 7750 50  0000 C CNN
F 1 "74LS00" H 3550 7675 50  0000 C CNN
F 2 "" H 3550 7475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 3550 7475 50  0001 C CNN
	1    3550 7475
	1    0    0    -1  
$EndComp
Text Label 3175 1175 2    50   ~ 0
~RDb
Wire Wire Line
	3175 1175 2775 1175
Text HLabel 2775 1175 0    50   Input ~ 0
~RDb
Wire Notes Line
	7775 1875 10475 1875
Wire Notes Line
	10475 1875 10475 5875
Wire Notes Line
	10475 5875 7775 5875
Wire Notes Line
	7775 5875 7775 1875
Text Notes 7825 2025 0    50   ~ 0
ECB - Bus
Wire Wire Line
	4700 2125 4725 2125
Wire Wire Line
	4725 5475 4725 3750
Wire Wire Line
	4725 5475 5500 5475
Connection ~ 4725 2125
Wire Wire Line
	4725 2125 5150 2125
Wire Wire Line
	4725 3750 5325 3750
Connection ~ 4725 3750
Wire Wire Line
	4725 3750 4725 2125
Wire Wire Line
	5750 850  5750 575 
Wire Wire Line
	5750 575  4725 575 
Wire Wire Line
	4725 575  4725 2125
Wire Wire Line
	5750 850  6125 850 
$EndSCHEMATC
