; --------------------------------------------------------
; INCLUDE TESTROUTINEN F�R EGRUND30
; --------------------------------------------------------

; Ausgaben k�nnen direkt mit PRTAC PRTHL PRTBIN ... erfolgen, 
; solange IX nicht ge�ndert wird. OUTTX druckt den Textbuffer 
; aus, ohne Register zu ver�ndern

typeSIMPL	.equ	0	; Einfache Testroutine 	 +  97 Byte
typeMENUE	.equ	1	; Test mit Men�steuerung + 230 Byte
testTYPE	.equ	typeSIMPL


#IF testTYPE = typeSIMPL
; einfaches Testprogramm ohne Menue

testprog:
	ld hl,testmsg1		; Header des Screens
	call textsetaus
	ld hl,testmsg2		; press any key - Message
	call textsetaus
	ld ix,ausbuf+6		; TextBuffer setzen

;-------------------------------------
; Hier folgt eine einfache Testroutine

;-------------------------------------

	call ki			; Taststurabfrage
	ret			; zum Hauptmenu
#ENDIF testTYPE


#IF testTYPE = typeMENUE
; Testprogramm mit Eintastenmen�

testmen:		; Men�definition
	.word 320,200\	.byte $21,0\	.text "1 --"\		.byte 0
	.word 320,184\	.byte $21,0\	.text "2 --"\		.byte 0
	.word 320,168\	.byte $21,0\	.text "3 --"\ 		.byte 0
	.word 320,152\	.byte $21,0\	.text "4 --"\ 		.byte 0

	.word 320,56\	.byte $21,0\	.text "0 CLS"\ 		.byte 0
	.word 320,40\	.byte $21,0\	.text "X EXIT"\ 	.byte 0
	.word $FFFF

testkey:		; Keytable
	.byte '1'\	.word testdummy
	.byte '2'\	.word testdummy
	.byte '3'\	.word testdummy
	.byte '4'\	.word testdummy
	.byte '0'\	.word testcls		; cls immer verf�gbar
	.byte 'X'\	.word restart		; exit immer verf�gbar
	.byte 0

;--------------------------------------
; Hier Unterprogramme f�r Men� codieren

;--------------------------------------


testcls:		; Bildschirm l�schen
	call clrall
	ld hl,testmsg1	; Aufbau neues Menue
	call textmulti
	ld ix,ausbuf+6	; TextBuffer setzen
testdummy:
	ret
		

testprog:		; Men�handling
	ld ix,ausbuf+6	; TextBuffer setzen
trestart:
	push ix
	call clrall 	;loeschen aller Seiten
	ld hl,testmsg1	;Aufbau neues Menue
	call textmulti
	pop ix
tkeyloop:
	call ki		;Tastenwert nach Accu
	ld c,a		;nach register c sichern
	ld hl,testkey	;Tabelle der Keycodes und Adressen
ttabloop:
	ld a,(hl)	;keycode aus tabelle lesen
	cp 0		;ende-code?
	jr z,tkeyloop	;neue taste holen
	cp c		;mit Tastenwert vergleichen
	jr z,tkeyfound	;gefunden, Adresse anspringen
	inc hl		;Zeiger auf n�chsten code
	inc hl
	inc hl
	jr ttabloop	;Tabelle abarbeiten
tkeyfound:
	inc hl		;Pointer auf Adresse
	ld e,(hl)	;Adresse auslesen
	inc hl
	ld d,(hl)
	ld h,d		; nach hl
	ld l,e
	push hl		;hl sichern auf stack
	ld hl,tkeyloop	;r�cksprungadresse nach hl
	ex (sp),hl	;und tauschen (ret -> restart)
	jp (hl)		;JP nach Routine

#ENDIF testType


;---------------------------------------
; Routinen und Daten f�r beide Testarten
outtx:			; Ausgabe des Textbuffers
	push af\ push bc\ push de\ push hl\ push ix\ push iy
	ld hl,0
	ld de,200
	ld a,$11
	call printbuf
	pop iy\ pop ix\ pop hl\ pop de\ pop bc\ pop af
	ret
testmsg1:
	.word 0,240
	.byte 32h,0
	.text "Test"
	.byte 0
testmsg2:
	.word 0,0
	.byte 21h,0
	.text "Press any key"
	.byte 0


; --------------------------------------------------------
; ENDE INCLUDE TESTROUTINEN F�R EGRUND30
; --------------------------------------------------------
