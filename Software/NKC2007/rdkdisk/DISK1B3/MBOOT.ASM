;*************************************************
;* Cold - Boot Programm fuer Minilaufwerke       *
;* Rolf-Dieter Klein 821130    1.0               *
;*************************************************

; das programm wird auf sektor 1 track 0 abgelegt


	org 80h		;anfangsadresse

cpmb	equ	0d400h	;start cp/m
bios	equ	0ea00h	;start bios

WBOOT0:	

	lxi sp,100h	;start of stack

	lxi h,0	
	mvi e,0
	mvi d,2		;mini steprate std BASF ...
	mvi c,0
	mvi b,0		;rstore und set step
	call 0f027h	;exec mini
;

	LXI H,CPMB	;START ADRESSE
	MVI B,34h/2	;ANZAHL DER SEKTOREN  A 256 BYTES
			;1a00h bytes
	MVI D,1		;START BEI TRACK 1 WEGEN DOUBLE DENSE
	MVI E,1		;1..16 SEKTOR
RDSEC:	
	PUSH H
	PUSH D
	PUSH B
	MVI C,11010000b	;laufwerk 0 double dense mini
	MVI B,1		;read  bei sysgen 2 setzen
	CALL 0F027H
	POP B
	POP D
	POP H
	JC 0f01eh	;monitor aufrufen bei fehler
	PUSH D		;NEXT ADRESSE
	LXI D,256	;double dense bloecke
	DAD D
	POP D
	INR E		;SEKTORANZAHL + 1
	MOV A,E
	CPI 16+1	;1..16  erst 17 ist nicht mehr ok 
	JC RD1
	MVI E,1		;START OVER
	INR D
RD1:	DCR B
	JNZ RDSEC
	jmp bios	;start coldbios

	end


